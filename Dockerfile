FROM python:3.9-slim


# Install system dependencies
RUN apt update \
	&& apt install -y git gcc ffmpeg libsm6 libxext6 libpq5 \
 	&& pip install waitress \
	&& git clone --depth 1 --branch 0.1.0 https://github.com/meituan/YOLOv6 /opt/YOLOv6 \
    && pip install torch torchvision --extra-index-url https://download.pytorch.org/whl/cpu \
    && pip install -r /opt/YOLOv6/requirements.txt \
	&& apt remove -y gcc \
 	&& rm -rf /var/lib/apt/lists/* \
	&& mkdir -p /opt/360app/server /opt/360app/viewer /data/360app

WORKDIR /opt/360app/server

# Install Python dependencies
COPY ./server/requirements.txt ./server/requirements-blur.txt ./
RUN pip install -r ./requirements.txt && \
    pip install -r ./requirements-blur.txt


# Add server source files
WORKDIR /opt/360app
COPY ./images ./images/
COPY ./docker/docker-entrypoint.sh ./
RUN chmod +x ./docker-entrypoint.sh
COPY ./server ./server/


# Environment variables
ENV DB_URL="postgres://user:pass@host/dbname"
ENV FS_URL="osfs:///data/360app"
ENV MODELS_FS_URL="/data/360models"
ENV SECRET_KEY="see_readme"
ENV YOLOV6_PATH="/opt/YOLOv6"
ENV BACKEND_MODE="api"
ENV BLUR_STRATEGY="FAST"
ENV DERIVATES_STRATEGY="ON_DEMAND"
ENV WEBP_METHOD="6"


# Expose service
EXPOSE 5000
ENTRYPOINT ["./docker-entrypoint.sh"]
