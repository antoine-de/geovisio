# GeoVisio

![GeoVisio logo](./images/logo_full.png)

GeoVisio is a complete solution for storing and __serving your own geolocated pictures__ (like [StreetView](https://www.google.com/streetview/) / [Mapillary](https://mapillary.com/)).

Give it a try at [geovisio.fr](https://geovisio.fr/viewer) !

This repository only contains __the backend and web API__. [All other components are listed here](https://gitlab.com/geovisio).


## Features

* A __web API__ to search / query the pictures collection
  * Search pictures by ID, date, location
  * Compatible with [SpatioTemporal Asset Catalog](https://stacspec.org/) and [OGC WFS 3](https://github.com/opengeospatial/WFS_FES) specifications
* An easy-to-use __backend__
  * Reads through your pictures collection and creates metadata in database
  * Generates automatically thumbnail, small and tiled versions of your pictures
  * Offers blurring of people and vehicle to respect privacy laws
  * Serves a default website and viewer


## Install & run

Our [documentation](./docs/) will help you install, configure and run a GeoVisio instance.

If at some point you're lost or need help, you can contact us through [issues](https://gitlab.com/PanierAvide/geovisio/-/issues) or by [email](mailto:panieravide@riseup.net).


## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

More information about developing is available in [documentation](./docs/).


## Special thanks

![Sponsors](./images/sponsors.png)

GeoVisio was made possible thanks to a group of ✨ __amazing__ people ✨ :

- __[GéoVélo](https://geovelo.fr/)__ team, for 💶 funding initial development and for 🔍 testing/improving software
- __[Carto Cité](https://cartocite.fr/)__ team (in particular Antoine Riche), for 💶 funding improvements on viewer (map browser, flat pictures support)
- __Stéphane Péneau__, for his work on 📷 picture blurring and sharing his 🧙 wisdom on pictures processing
- __Albin Calais__ (mentored by Cyrille Giquello), for implementing 🎚️ multiple picture blurring strategies
- __[Damien Sorel](https://www.strangeplanet.fr/)__, for developing the 📷 amazing [Photo Sphere Viewer](https://photo-sphere-viewer.js.org/) on which GeoVisio viewer heavily depends, and making it evolve to better fit our needs
- __Antoine Desbordes__, for adding a simple way to test Geovisio without having many 📷 pictures available
- __Pascal Rhod__, for improving 🐋 Docker configuration file
- __Nick Whitelegg__, for working on the convergence of GeoVisio and [OpenTrailView](https://opentrailview.org/)
- __[Adrien Pavie](https://pavie.info/)__, for ⚙️ initial development of GeoVisio
- And you all ✨ __GeoVisio users__ for making this project useful !


## License

Copyright (c) Adrien Pavie 2022-2023, [released under MIT license](./LICENSE).
