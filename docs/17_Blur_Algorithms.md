# Picture blurring

Picture blurring can be enabled using the `BLUR_STRATEGY` environment variable, by setting it to one of `FAST`, `COMPROMISE`, `QUALITATIVE` and `LEGACY`.

New pictures will be blurred before being inserted into the database according to the blur strategy. The strategy *can* be changed but previously processed pictures will not be re-processed unless you run `redo-sequences` command.

If you use blur with __Docker__, you may want to map container folder `/data/360models` to an host folder where there is enough space to store blurring models.

## Options

Several specific options are available, and can be defined like others either through `config.py` file or using environment variables.

- `BLUR_STRATEGY` (optional) : set algorithm to use for blurring, it depends on the level of speed and precision you require. It doesn't automatically re-process previously uploaded sequences, but you can use `redo-sequences` command to do so. Values are:
  - `DISABLE` completely disables blurring
  - `FAST` is the fastest algorithm but should be considered unreliable on pictures with lots of persons or large pictures like 360° images with persons in the background
  - `COMPROMISE` should be reliable enough for all kinds of images but the blur may be jagged
  - `QUALITATIVE` takes a lot more time to complete but will accomplish good detouring
  - `LEGACY` theorically has the best results but is *way* slower than every other method (this is the blur algorithm used in GeoVisio versions <= 1.2.0)
- `MODELS_FS_URL` (optional) : the path to AI models files (defaults to `./server/src/blur/models`).
- `BLUR_THREADS_LIMIT` (optional) : limits the number of threads the blur algorithm can use. If set to 0 the process will use all available cpu resources. Such limit may be required on lower-end computers, otherwise the process might crash. It defaults to 1.

If blurring is too slow for your needs, you may try to increase the number of threads with `BLUR_THREADS_LIMIT=n`, try not to set `n` too high because the AI models used require lots of RAM & CPU resources. If at any point your CPU usages skyrockets and the program crashes with a line such as "process: 9 Killed", it is your OS telling you that the process took too much RAM.

You can change of strategy at anytime. Choosen strategy will be used for next sequences you will process. If you want to apply this strategy to previously imported sequences, you can use the `redo-sequences <seq1> <seq2>...` command.

## Development notes

This part presents more information about blur strategies implementation, this is a recommended-but-not-mandatory read 😉

### General blur pipeline

The blurring pipeline is as follow:

1. Get an input image
2. Split it into multiple quads, which sizes correspond to the object detection AI model (inferer)'s input size
3. Give the quads to the inferer
4. Get boxes that *may* contain cars, persons...
5. Place them on a new single image, which size is as close as possible to the sementic segmentation AI model (segmenter)'s input size
6. Give that image to the segmenter
7. Get a blur mask back (containing a blur mask for each box)
8. Arrange the blur masks back on the original image, given the inferer's boxes
9. Apply the blur mask to the original image

The inferer is [YOLOv6](https://github.com/meituan/YOLOv6) and the segmenter depends on the segmentation strategy.

When using the `FAST` strategy, steps 2-5 and 8 are skipped and inferer is not used.

On step 6 (no matter if steps 2-5 took place), if the image is larger than the segmenter's input size, it is reduced to fit in width *or* height and fed to the segmenter in one or more batches.

The whole process can be multithreaded, depending on the `BLUR_THREADS_LIMIT` environment variable. Note that some AI models cannot be shared and require that they be loaded once on each thread.

### Logs

There are several log messages comming from Tensorflow, Pytorch and YOLO that cannot be easily suppressed :\
`INFO: Created TensorFlow Lite XNNPACK delegate for CPU.`, see [a relating issue](https://github.com/google/mediapipe/issues/2354). To fix, a Tensorflow build from source is required.\
`...UserWarning: torch.meshgrid: in an upcoming release, it will be required to pass the indexing argument...`, this is a YOLOv6 generated warning, see [a fix](https://github.com/pytorch/pytorch/issues/50276). To fix, add `, indexing='ij'` in the calls to `torch.meshgrid()` in files `effidehead.py` and `loss.py` of YOLOv6's source.

### GPU/TPU usage

To run the segmenter on the GPU, a CUDA capable GPU is required, run `pip uninstall tensorflow && pip install tensorflow-gpu`. For more information see [the Tensorflow GPU guide](https://www.tensorflow.org/guide/gpu).

To run the inferer on the GPU, refer to the [Pytorch download page](https://pytorch.org/) to find the right Pytorch version with CUDA capabilities.
Both the inferer and segmenter should automatically switch to GPU, this __hasn't been tested__ in a production environment. If you see any issues, please [let us know](https://gitlab.com/PanierAvide/geovisio/-/issues).

For TPU usage, refer to the [Tensorflow tpu guide](https://www.tensorflow.org/guide/tpu), Pytorch seems to be compatible with TPUs but it remains to be verified whether YOLO's architectures also is.

### On YOLOv6 updates

Current GeoVisio code and documentation fixes YOLOv6 to its 0.2.0 release. This has been done because trained models are not compatible through versions, and should be explicitly changed in some parts of the code. When updating, make sure to update models download links and YOLOv6 release tag everywhere necessary in GeoVisio code.

## Next step

You may want to know more about [developing on the server](./19_Develop_server.md).
