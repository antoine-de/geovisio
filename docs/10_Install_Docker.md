# Install Geovisio using Docker

The [Docker](https://docs.docker.com/get-docker/) deployment is a really convenient way to have a Geovisio instance running in an easy and fast way. If you prefer to not use Docker, you can see [classic install documentation](./10_Install_Classic.md).

Available Docker configuration files are :

* [Dockerfile](../Dockerfile) : contains API, blur algorithms and JS client. An __external__ PostgreSQL database must be provided.
* [docker-compose.yml](../docker-compose.yml) : offers everything (API, blur, JS client, database) using __Docker Hub__ `geovisio/api:develop` image

__Contents__

[[_TOC_]]


## Install methods

### Run with Dockerfile

In this setup, API, blur algorithms and JS client are provided. A database must be available somewhere, see [database setup documentation](./07_Database_setup.md) to set it up, or use one of the Docker compose setup offered below.

You can use the provided __Docker Hub__ `geovisio/api:develop` image directly:

```bash
docker run \
	-e DB_URL=<database connection string> \
	-p 5000:5000 \
	--name geovisio \
	-v <path to a local directory with pictures>:/data/360app \
	geovisio/api:develop
```

You can also build the image from the local source with:

```bash
docker build -t geovisio/api:latest .
```

In both cases, this will run a container bound on port 5000 and use local images from the provided volume.

### Run with docker-compose.yml

In this setup, everything is available without any other external components.

This setup uses the __Docker Hub__ `geovisio/api:develop` image, meaning you don't have to build it locally. This is useful for a faster deployment, but don't allow to see changes to local source code. If you want to test easily your source code changes, use [docker-compose-dev.yml](#run-with-docker-compose-dev.yml) instead.

```bash
# Create the "pictures" folder
mkdir ./pictures

# Install
docker-compose pull
```

### Use the local Dockerfile

In this setup, everything is available without any other external components.

This setup uses the __local Dockerfile__ of Geovisio, meaning you can use it for development and test your source code changes. This takes more time as image is built locally. If you want to deploy faster without needing to change source code, use the __Docker Hub__ `geovisio/api:develop` image.

```bash
# Create the "pictures" folder
mkdir ./pictures

# Install Geovisio
docker-compose build
```


## Next step

You can read more about [server settings](./11_Server_settings.md).
