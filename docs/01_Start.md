# GeoVisio hands-on guide

![GeoVisio logo](../images/logo_full.png)

Welcome to GeoVisio __API__ documentation ! It will help you through all phases of setup, run and develop on GeoVisio API and backend.

If you're looking for the GeoVisio _Viewer_ documentation, it is [available here](https://gitlab.com/geovisio/web-viewer/-/tree/develop/).

Also, if at some point you're lost or need help, you can contact us through [issues](https://gitlab.com/geovisio/geovisio/-/issues) or by [email](mailto:panieravide@riseup.net).

You might want to dive into docs :

- Deploy an instance
  - [Using Docker](./10_Install_Docker.md)
  - Using a classic install
	 - [Setup your database](./07_Database_setup.md)
	 - [Install Geovisio and dependencies](./10_Install_Classic.md)
  - [Using Scalingo solutions](./10_Install_Scalingo.md)
- [Available settings](./11_Server_settings.md)
- [Organize your pictures and sequences](./12_Pictures_storage.md)
- Start and use your server
  - [Classic-deployed](./15_Running_Classic.md)
  - [Docker-deployed](./15_Running_Docker.md)
- [Work with HTTP API](./16_Using_API.md)
- Advanced server topics
  - [Available blur algorithms](./17_Blur_algorithms.md)
  - [Developing on the server](./19_Develop_server.md)
