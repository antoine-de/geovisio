# Using the HTTP API

By default, your Geovisio HTTP API is available on [localhost:5000](http://localhost:5000/).

The HTTP API allows you to access to collections (sequences), items (pictures) and their metadata. Geovisio API is following the [STAC API specification](https://github.com/radiantearth/stac-api-spec/blob/main/overview.md), which is based on OGC API and a widely-used standard for geospatial imagery. More info about STAC [is available online](https://stacspec.org/en).

API routes are documented at [localhost:5000/apidocs](http://localhost:5000/apidocs) ([online version](https://geovisio.fr/apidocs)).

## Authentication

If activated at the GeoVisio's instance level, some routes might need authentication.

The authentication is asked to the configured instance's OAuth provider and stored in a session cookie.

The routes that need authentication should redirect to the `/api/auth/login` route if no session cookie is set.

A logout from the instance can be done via the `/api/auth/logout` route. This logout only logout from geovisio, not from the identity provider (if it's a problem, please open an issue to discuss it).

## Upload

GeoVisio also offers API routes to upload pictures and sequences. Note that this API may change in a near future, as it is quite new.

Upload should go through different steps:

1. Create a new sequence with `POST /api/collections`. It will give you back a sequence ID that you will need for next steps.
2. Upload as many pictures as wanted with `POST /api/collections/<SEQUENCE ID>/items`. Both a JPEG image file and a numeric position in sequence are needed.
3. Wait a bit for server to process (save, read metadata and blur) your pictures. You can check status using `GET /api/collections/<SEQUENCE ID>/geovisio_status` to see the progress made on process.
4. Enjoy your brand-new uploaded sequence with `GET /api/collections/<SEQUENCE ID>/items` !

You can go through these steps with the following _cURL_ bash commands (considering your GeoVisio server is `https://geovisio.fr/`):

```bash
# Create collection
# You will have in both Location HTTP response header
# and in JSON response body the sequence ID
curl \
	-i -d "title=My%20sequence" \
	https://geovisio.fr/api/collections

# We consider below sequence ID is 60d94628-8098-42cc-b684-ffb9aa9d35a7

# Send a first picture
# You will have in both Location HTTP response header
# and in JSON response body the picture ID
curl \
	-i -F "picture=@my_picture_001.jpg" \
	-F "position=1" \
	https://geovisio.fr/api/collections/60d94628-8098-42cc-b684-ffb9aa9d35a7/items

# Send as many pictures as you want...
# Just mind changing the position parameter

# Check processing status
# If all items are ready, you're good to go !
curl https://geovisio.fr/api/collections/60d94628-8098-42cc-b684-ffb9aa9d35a7/geovisio_status

# Get whole sequence as GeoJSON
curl https://geovisio.fr/api/collections/60d94628-8098-42cc-b684-ffb9aa9d35a7/items
```

## Next step

At this point, you might either be interested in:

- [Use the Geovisio web client](./20_Client_quickstart.md)
- [Learn more about available blur algorithms](./17_Blur_algorithms.md)
- [Make developments on the server](./19_Develop_server.md)
