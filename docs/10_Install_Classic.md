# Install Geovisio on a classic server

Geovisio can be deployed manually on a classic server, offering best performance and high-level of customization. If you prefer a more _plug & play_ deployment, you can look at the [Docker install documentation](./10_Install_Docker.md).

## Dependencies

GeoVisio best runs with a recent Linux server and needs these components:

- Python __3.9__, higher versions are currently unsupported by some blurring algorithms
- An up and running database, as seen in [database setup documentation](./07_Database_setup.md)
- Specific dependencies according to pictures blur strategy used

## Install

You have to run following commands for installing classic dependencies:

```bash
# Enable Python environment
python3 -m venv env
source ./env/bin/activate

# Install Python dependencies
pip install -r requirements.txt
pip install -r requirements-blur.txt # Only if you want to enable pictures blurring
pip install -r requirements-dev.txt # Only for dev or testing purposes
```

If you need _good_ blur algorithms (`COMPROMISE` or `QUALITATIVE` strategies), you need the following dependencies as well:

```bash
# Install pytorch, the exact command depends on the os and pytorch version
# See https://pytorch.org/ to get the good one for you
# As an example, this is the commande for Linux OS without GPU
pip install torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cpu

# Download YOLOv6, an object detection AI
git clone --depth 1 --branch 0.2.0 https://github.com/meituan/YOLOv6 /opt/YOLOv6
pip install -r /opt/YOLOv6/requirements.txt

# Make YOLOv6 accessible through an environment variable
export YOLOV6_PATH=/opt/YOLOv6
```

## Next step

You can read more about [server settings](./11_Server_settings.md).
