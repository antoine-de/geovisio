# Available commands on Docker deployment

Various operations can be run on GeoVisio server : reading your pictures and sequences, start API, migrate or clean-up database... This documentation cover available commands on a Docker instance, if you're using a classic deployment please [see this doc instead](./15_Running_Classic.md).

[[_TOC_]]

## How to run commands

According to your setup, you might want to launch commands a bit differently.

### Single Docker container

```bash
# Set env variables according to your setup
export DB_URL="postgres://user@host:port/db"

# Run container
docker run --rm \
	-e DB_URL \
	-v /my/pic/dir:/data/360app \
	geovisio/api \
	<COMMAND TO RUN>
```

### Docker Compose

```bash
# Start whole environment
docker-compose -f <COMPOSE YML FILE> up

# Run a command on backend
docker-compose -f <COMPOSE YML FILE> run --rm backend <COMMAND TO RUN>
```


## Available commands

```
api                 Starts web API for production
ssl-api             Starts web API for production with SSL enabled
dev-api             Starts web API for development
process-sequences   Starts analyzing pictures metadata
db-upgrade          Database migration
redo-sequences      Re-process sequence files
cleanup             Cleans database and remove Geovisio derivated files
```

### Start API

Commands `api`, `ssl-api` and `dev-api` allows you to start Geovisio API, either on HTTP, HTTPS or development mode.

### Process pictures & sequences

The `process-sequences` command reads all available sequences and pictures in your filesystem (as pointed by `FS_URL` paramater) to import them in database and run blur algorithm (if enabled by `BLUR_STRATEGY` parameter).

### Database migration

As GeoVisio is actively developed, when updating from a previous version, some database migration could be necessary. If so, when starting GeoVisio, an error message will show up and warn about necessary migration. The `db-upgrade` command has to be ran then.

### Re-process some sequences

If you need to re-process one or several sequences (to re-do pictures blurring with another strategy for example, or after fixing metadata of some pictures), you can use the `redo-sequences` command followed by sequences folder names:

```bash
<DOCKER COMMAND> redo-sequences <SEQUENCE_FOLDERNAME_1> <SEQUENCE_FOLDERNAME_2> ...
```

### Clean-up

Eventually, if you want to clear database and delete derivate versions of pictures (it __doesn't__ delete original pictures), you can use the `cleanup` command. You can also run some partial cleaning with the same cleanup command and one of the following options:

```bash
<DOCKER COMMAND> cleanup \
    --database \ # Removes entries from database, but not picture derivates
    --cache \ # Only removes picture derivates, but not blur masks
    --blur-masks # Only removes blur masks
```

### Other commands

Additional commands can be available (as documented in [classic instance running doc](./15_Running_Classic.md)). These commands can also be launched on a Docker instance using the following syntax:

```bash
# Docker container
docker exec \
	-w /opt/360app/server \
	<CONTAINER NAME> \
	flask <GEOVISIO COMMAND>

# Docker compose
docker-compose -f <COMPOSE YML FILE> \
	exec -w /opt/360app/server \
	backend \
	flask <GEOVISIO COMMAND>
```


## Running with authentication activated

To run geovisio with a configured keycloak, you can use the provided docker-compose files.

Note that the provided Keycloak configuration __should not be used in production__, it's only for testing.

There are 2 possibilities.

### Running all with docker-compose

For this you can use the docker-compose file `docker/docker-compose-auth.yml`.

```bash
docker-compose -f docker/docker-compose-auth.yml up
```

You can also use the provided makefile for this:
```bash
make run-all-compose
```


### Running keycloak with a local flask server

As this is more for development purpose, this part is explained in the corresponding [Develop server section](./19_Develop_server.md#keycloak).

## Next step

Your server is up and running, you can [work with the API](./16_Using_API.md).
