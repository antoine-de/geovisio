# Avaible Geovisio server settings

GeoVisio server is highly configurable. Depending on your install, you may want to pass settings through different methods:

- Using environment variables (`-e` option for Docker commands)
- Using [python-dotenv](https://github.com/theskumar/python-dotenv) and either using the default `.env` file or providing path to a custom .env file to Flask with the `--env-file` (`-e`) flag (cf [Flask's documentation](https://flask.palletsprojects.com/en/2.2.x/cli/?highlight=dotenv#environment-variables-from-dotenv))

### Mandatory parameters

The following parameters must always be defined, otherwise Geovisio will not run. Note that they are automatically set-up when using __Docker compose__ install.

- `DB_URL` : [connection string](https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING) to access the PostgreSQL database. You can alternatively use a set of `DB_PORT`, `DB_HOST`, `DB_USERNAME`, `DB_PASSWORD`, `DB_NAME` parameters to configure database access.
- `FS_URL` : file system and directory to use for reading and storing pictures (in [PyFilesystem format](https://docs.pyfilesystem.org/en/latest/openers.html)), for example : `osfs:///path/to/pic/dir` (disk storage), `s3://mybucket/myfolder?endpoint_url=https%3A%2F%2Fs3.fr-par.scw.cloud&region=fr-par` (S3 storage)

### Optional parameters

#### General parameters
- `BLUR_STRATEGY` : activate blurring for all new picture uploads. See [blur documentation](./17_Blur_Algorithms.md) for more info.
- `DERIVATES_STRATEGY` : sets if picture derivates versions should be pre-processed (value `PREPROCESS` to generate all derivates during `process-sequences`) or created on-demand at user query (value `ON_DEMAND` by default to generate at first API call needing the picture).
- `BACKEND_MODE` : sets the mode to run GeoVisio into ("api" for web API, "process-sequences" for checking and processing incoming pictures). This is used mainly for Kubernetes / Dockerized deployments to avoid passing arguments to container.
- `MAIN_PAGE` and `VIEWER_PAGE` : changes the template to use for display main and viewer page. This should be the name of an existing HTML template in `/server/src/templates` folder, for example `main.html` or `viewer.html`.
- `WEBP_METHOD` : quality/speed trade-off for WebP encoding of pictures derivates (0=fast, 6=slower-better, 6 by default).
- `PICTURE_PROCESS_THREADS_LIMIT`: limit the number of thread used to process pictures. If set to 0, the limit is the available number of threads (default to 1).
- `WEBP_CONVERSION_THREADS_LIMIT`: limit the number of thread used to convert a picture to webp. If set to 0, the limit is the available number of threads (default to 0).

#### Cookie management
- `SECRET_KEY`: [Flask's secret key](https://flask.palletsprojects.com/en/2.2.x/config/#SECRET_KEY). A secret key used among other things for securely signing the session cookie. For production should be provided with a long random string as stated in flask's documentation.
- `PERMANENT_SESSION_LIFETIME`: [Flask's cookie lifetime](https://flask.palletsprojects.com/en/2.2.x/config/#PERMANENT_SESSION_LIFETIME), number of second before a cookie is invalided (and thus time before the user should log in again). Default is 7 days.
- `SESSION_COOKIE_DOMAIN`: [Flask's cookie domain](https://flask.palletsprojects.com/en/2.2.x/config/#SESSION_COOKIE_DOMAIN), should be set to the domain of the instance.

#### Oauth configuration
- `OAUTH_PROVIDER`: external oauth provider used to authenticate users. If provided can be either `oidc` (for [OpenIdConnect](https://openid.net/connect/)) or `osm`.
- `OIDC_URL`: if `OAUTH_PROVIDER` is `oidc`, url of the realm to use (example: `http://localhost:3030/realms/geovisio` for a keycloack deployed locally on port `3030`)
- `CLIENT_ID`: oauth client id as set in the identity provider
- `CLIENT_SECRET`: oauth client secret as set in the identity provider
- `FORCE_AUTH_ON_UPLOAD`: require a login before using the upload api (collection creation and pictore upload). This requires that an `OAUTH_PROVIDER` has been set. 

#### Infrastructure
- `NB_PROXIES` : tell geovisio that is is run behind some proxies, so that it can trust the `X-Forwarded-` headers for url generations (more information in the [flask documentation](https://flask.palletsprojects.com/en/2.2.x/deploying/proxy_fix/)).

## Next step

You can now check how to [add an external identity provider](./12_External_Identity_Providers.md).
