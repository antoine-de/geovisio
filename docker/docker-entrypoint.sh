#!/bin/bash

usage() {
    echo "./docker-entrypoint.sh <COMMAND>: "
    echo -e "\tThis script simplifies running GeoVisio backend in a certain mode"
    echo "Commands: "
    echo -e "\tapi: Starts web API for production on port 5000 by default"
    echo -e "\tdev-api: Starts web API for development on port 5000 by default"
    echo -e "\tprocess-sequences: Starts analyzing pictures metadata"
    echo -e "\tredo-sequences <sequences>: Re-process sequence files"
    echo -e "\ttest: Test suite"
    echo -e "\ttest-ci: Test suite (skipping heavy processes)"
    echo -e "\tcleanup: Cleans database and remove Geovisio derivated files (it doesn't delete your original pictures)"
    echo -e "\tdb-upgrade: Upgrade the database schema"
}

# Check if BACKEND_MODE env var exists if no explicit argument given
if [ "${1}" == "" ]; then
    command=${BACKEND_MODE}
else
    command=${1}
    shift
fi

cwd=$(realpath $(dirname "${BASH_SOURCE[0]}"))
cd "$cwd/server"

echo "Executing \"${command}\" command"

case $command in
"api")
    python3 -m waitress --port 5000 --call 'src:create_app'
    ;;
"ssl-api")
    python3 -m waitress --port 5000 --url-scheme=https --call 'src:create_app'
    ;;
"dev-api")
    python3 -m flask --debug run --host=0.0.0.0
    ;;
"process-sequences")
    python3 -m flask process-sequences
    ;;
"redo-sequences")
    python3 -m flask redo-sequences "$@"
    ;;
"cleanup")
    python3 -m flask cleanup
    ;;
"db-upgrade")
    python3 -m flask db upgrade
    ;;
"test")
    python3 -m pytest
    ;;
"test-ci")
    python3 -m pytest -m "not skipci"
    ;;
*)
    usage
    ;;
esac

cd "$cwd"
