import os
import psycopg
import re
import configparser
import click
import requests
import json
from flask import Flask, g, jsonify, render_template, send_from_directory, redirect
from flask.cli import with_appcontext
from flask_cors import CORS
from flask_compress import Compress
from flasgger import Swagger
from fs import open_fs
from . import pictures, runner_pictures, errors, stac, map, db_migrations, auth, users


def create_app(test_config=None, app=None):
	"""API launcher method"""

	#
	# Create and setup Flask App
	#

	if app is None:
		app = Flask(__name__, instance_relative_config=True)
	CORS(app)
	Compress(app)

	# Override eventually from environment variables
	confFromEnv = [
		'FS_URL',
		'DB_URL',
		'DB_PORT',
		'DB_HOST',
		'DB_USERNAME',
		'DB_PASSWORD',
		'DB_NAME',
		'SECRET_KEY',
		'BLUR_STRATEGY',
		'BLUR_THREADS_LIMIT',
		'YOLOV6_PATH',
		'MODELS_FS_URL',
		'VIEWER_PAGE',
		'MAIN_PAGE',
		'DERIVATES_STRATEGY',
		'WEBP_METHOD',
		'OAUTH_PROVIDER',
		'PERMANENT_SESSION_LIFETIME',
		'SESSION_COOKIE_DOMAIN',
		'OIDC_URL',
		'CLIENT_ID',
		'CLIENT_SECRET',
		'NB_PROXIES',
		'FORCE_AUTH_ON_UPLOAD',
		'PICTURE_PROCESS_THREADS_LIMIT',
		'WEBP_CONVERSION_THREADS_LIMIT',
	]
	for e in confFromEnv:
		if os.environ.get(e):
			app.config[e] = os.environ.get(e)


	# Create DB_URL from separated parameters
	if 'DB_PORT' in app.config or 'DB_HOST' in app.config or 'DB_USERNAME' in app.config or 'DB_PASSWORD' in app.config:
		username = app.config['DB_USERNAME'] if 'DB_USERNAME' in app.config else ""
		passw = app.config['DB_PASSWORD'] if 'DB_PASSWORD' in app.config else ""
		host = app.config['DB_HOST'] if 'DB_HOST' in app.config else ""
		port = app.config['DB_PORT'] if 'DB_PORT' in app.config else ""
		dbname = app.config['DB_NAME'] if 'DB_NAME' in app.config else ""

		app.config['DB_URL'] = f"postgres://{username}:{passw}@{host}:{port}/{dbname}"

	# Final overriding from test_config
	if test_config is not None:
		app.config.update(test_config)

	#
	# Set defaults for various env vars
	#

	# Default viewer page
	if app.config.get('VIEWER_PAGE') is None:
		app.config['VIEWER_PAGE'] = "viewer.html"

	# Default main page
	if app.config.get('MAIN_PAGE') is None:
		app.config['MAIN_PAGE'] = "main.html"

	if app.config.get('BLUR_STRATEGY') is None:
		app.config['BLUR_STRATEGY'] = 'FAST'
	elif app.config['BLUR_STRATEGY'] not in [ 'DISABLE', 'FAST', 'COMPROMISE', 'QUALITATIVE', 'LEGACY' ]:
		raise Exception("Unknown blur strategy: '%s'. Please set to one of DISABLE, FAST, COMPROMISE, QUALITATIVE, LEVACY" % app.config['BLUR_STRATEGY'])

	if app.config.get('DERIVATES_STRATEGY') is None:
		app.config['DERIVATES_STRATEGY'] = 'ON_DEMAND'
	elif app.config['DERIVATES_STRATEGY'] not in ['ON_DEMAND', 'PREPROCESS']:
		raise Exception("Unknown picture derivates strategy: '%s'. Please set to one of ON_DEMAND, PREPROCESS" % app.config['DERIVATES_STRATEGY'])

	if app.config['BLUR_STRATEGY'] != 'DISABLE' and 'MODELS_FS_URL' not in app.config:
		app.config['MODELS_FS_URL'] = '../models'

	if app.config.get('BLUR_THREADS_LIMIT') is None:
		app.config['BLUR_THREADS_LIMIT'] = 1
	else:
		app.config['BLUR_THREADS_LIMIT'] = int(app.config['BLUR_THREADS_LIMIT'])
		if app.config['BLUR_THREADS_LIMIT'] < 0:
			app.config['BLUR_THREADS_LIMIT'] = 0

	if app.config.get('WEBP_METHOD') is None or app.config.get('WEBP_METHOD') == "":
		app.config['WEBP_METHOD'] = 6
	else:
		try:
			app.config['WEBP_METHOD'] = int(app.config['WEBP_METHOD'])
			if app.config['WEBP_METHOD'] not in [0,1,2,3,4,5,6]:
				raise ValueError()
		except ValueError:
			raise Exception("Invalid WebP method value: '%s'. Valid values are integers between 0 and 6." % app.config['WEBP_METHOD'])

	if app.config.get('WEBP_CONVERSION_THREADS_LIMIT') is None:
		# a limit to 0 means the limit is `os.cpu_count()`
		app.config['WEBP_CONVERSION_THREADS_LIMIT'] = 0
	app.config['WEBP_CONVERSION_THREADS_LIMIT'] = _get_threads_limit(app.config['WEBP_CONVERSION_THREADS_LIMIT'])
  
	if app.config.get('PICTURE_PROCESS_THREADS_LIMIT') is None:
		# a limit to 0 means the limit is `os.cpu_count()`
		# The default is to use only one only 1 thread to process uploaded pictures
		app.config['PICTURE_PROCESS_THREADS_LIMIT'] = 1
  
	nb_proxies = app.config.get('NB_PROXIES')
	if nb_proxies:
		nb_proxies = int(nb_proxies)
		# tell flask that it runes behind NB_PROXIES proxies so that it can trust the `X-Forwarded-` headers
		# https://flask.palletsprojects.com/en/2.2.x/deploying/proxy_fix/
		from werkzeug.middleware.proxy_fix import ProxyFix
		app.wsgi_app = ProxyFix(
			app.wsgi_app, x_for=nb_proxies, x_proto=nb_proxies, x_host=nb_proxies, x_prefix=nb_proxies
		)
  
	#
	# Add generated config vars
	#

	app.url_map.strict_slashes = False
	app.config['PIC_DERIVATES_DIR'] = "/geovisio_derivates"
	app.config['COMPRESS_MIMETYPES'].append("application/geo+json")
	app.config['EXECUTOR_MAX_WORKERS'] = _get_threads_limit(app.config['PICTURE_PROCESS_THREADS_LIMIT'])
	app.config['EXECUTOR_PROPAGATE_EXCEPTIONS'] = True # propagate the excecutor's exceptions, to be able to trace them

	# Prepare filesystem
	createDirNoFailure(app.instance_path)
	pic_fs = open_fs(app.config['FS_URL'])

	# Compatibility check for derivates folder
	# Versions <= 1.3.0 had this folder named gvs_derivates
	# So we check if old naming exists and rename if so
	oldPicDerivatesDir = "/gvs_derivates"
	if pic_fs.isdir(oldPicDerivatesDir):
		print("Updating derivates folder name...")
		pic_fs.movedir(oldPicDerivatesDir, app.config['PIC_DERIVATES_DIR'], create=True)
	else:
		pic_fs.makedirs(app.config['PIC_DERIVATES_DIR'], recreate=True)

	# Check database connection and update its schema if needed
	db_migrations.update_db_schema(app.config['DB_URL'])

	if 'OAUTH_PROVIDER' in app.config:
		if 'PERMANENT_SESSION_LIFETIME' not in app.config:
			# we default we keep the session cookie 7 days, users would have to renew their loggin after this
			import datetime
			app.config['PERMANENT_SESSION_LIFETIME'] = datetime.timedelta(days=7).total_seconds()

		auth.make_auth(app)
		app.register_blueprint(auth.bp)
  
	
	authenticated_upload = app.config.get("FORCE_AUTH_ON_UPLOAD", False)
	if authenticated_upload and not 'OAUTH_PROVIDER' in app.config:
		raise Exception("Login cannot be required for upload if 'OAUTH_PROVIDER' has not been set, please define the oauth provider.")
	app.config['FORCE_AUTH_ON_UPLOAD'] = bool(authenticated_upload)
 
	#
	# API documentation
	#

	# Read API metadata from setup.cfg
	setupCfg = configparser.RawConfigParser()
	setupCfg.read(os.path.join(os.path.dirname(__file__), '../setup.cfg'))
	apiMeta = dict(setupCfg.items('metadata'))

	apiDocs = {
		"info": {
			"title": apiMeta['name'],
			"version": apiMeta['version'],
			"description": apiMeta['description'],
			"contact": {
				"name": apiMeta['maintainer'],
				"url": apiMeta['url'],
				"email": apiMeta['maintainer_email']
			}
		},
		"schemes": [
			"http",
			"https"
		],
		"definitions": {
			"Collections": {
				"type": "object",
				"required": [ "links", "collections" ],
				"properties": {
					"links": {"type": "array", "items": { "$ref": "#/definitions/Link" } },
					"collections": {"type": "array", "items": { "$ref": "https://schemas.stacspec.org/v1.0.0/collection-spec/json-schema/collection.json" } }
				}
			},
			"Link": {
				"type": "object",
				"required": ["href", "rel"],
				"properties": {
					"href": { "type": "string", "example": "http://data.example.com/buildings/123" },
					"rel": { "type": "string", "example": "alternate" },
					"type": { "type": "string", "example": "application/geo+json" },
					"title": { "type": "string", "example": "Trierer Strasse 70, 53115 Bonn" }
				}
			},
			"extent": {
				"title": "Extents",
				"type": "object",
				"required": [ "spatial", "temporal" ],
				"properties": {
					"spatial": {
						"title": "Spatial extent object",
						"type": "object",
						"required": ["bbox"],
						"properties": {
							"bbox": {
								"title": "Spatial extents",
								"type": "array",
								"minItems": 1,
								"items": {
									"title": "Spatial extent",
									"type": "array",
									"oneOf": [
										{ "minItems":4, "maxItems":4 },
										{ "minItems":6, "maxItems":6 }
									],
									"items": { "type": "number" }
								}
							}
						}
					},
					"temporal": {
						"title": "Temporal extent object",
						"type": "object",
						"required": [ "interval" ],
						"properties": {
							"interval": {
								"title": "Temporal extents",
								"type": "array",
								"minItems": 1,
								"items": {
									"title": "Temporal extent",
									"type": "array",
									"minItems": 2,
									"maxItems": 2,
									"items": {
										"type": [ "string", "null" ],
										"format": "date-time",
										"pattern": "(\\+00:00|Z)$"
									}
								}
							}
						}
					}
				}
			}
		}
	}
	swagger = Swagger(app, template=apiDocs)

	#
	# List available routes/blueprints
	#

	app.register_blueprint(pictures.bp)
	stac.executor.init_app(app)
	app.register_blueprint(stac.bp)
	app.register_blueprint(map.bp)
	app.register_blueprint(users.bp)

	# Main page
	@app.route('/')
	def index():
		return render_template(app.config['MAIN_PAGE'])

	# Viewer
	@app.route('/viewer')
	def viewer():
		return render_template(app.config['VIEWER_PAGE'])

	@app.route('/apidocs')
	def apidocsNoSlash():
		return redirect('/apidocs/', 301)

	@app.route('/static/img/<path:path>')
	def viewer_img(path):
		return send_from_directory(
			os.path.join(os.path.dirname(__file__), '../../images'),
			path
		)

	# Errors
	@app.errorhandler(errors.InvalidAPIUsage)
	def invalid_api_usage(e):
		return jsonify(e.to_dict()), e.status_code


	#
	# Add CLI functions
	#

	@app.cli.command("process-sequences")
	@with_appcontext
	def process_sequences():
		"""Starts processing of all new sequences.
		This reads picture EXIF metadata, imports in database,
		and eventually generates derivates versions of pictures according to your settings.
		"""
		runner_pictures.run()

	@app.cli.command("redo-sequences")
	@click.argument("sequences", nargs=-1)
	@with_appcontext
	def redo_sequences(sequences):
		"""Re-processes already imported sequences.
		This updates database and derivates according to changes in original picture files.
		"""
		runner_pictures.redoSequences(sequences)

	@app.cli.command("set-sequences-heading")
	@click.option("--value", show_default=True, default=0, help="Heading value relative to movement path (in degrees)")
	@click.option("--overwrite", is_flag=True, show_default=True, default=False, help="Overwrite existing heading values in database")
	@click.argument("sequences", nargs=-1)
	@with_appcontext
	def set_sequences_heading(sequences, value, overwrite):
		"""Changes pictures heading metadata.
		This uses the sequence movement path to compute new heading value.
		"""
		runner_pictures.setSequencesHeadings(sequences, value, overwrite)

	@app.cli.command("cleanup")
	@click.option("--full", is_flag=True, show_default=True, default=False, help="For full cleanup (DB, cache, blur masks)")
	@click.option("--database", is_flag=True, show_default=True, default=False, help="Deletes database entries")
	@click.option("--cache", is_flag=True, show_default=True, default=False, help="Deletes cached derivates files (except blur masks)")
	@click.option("--blur", is_flag=True, show_default=True, default=False, help="Deletes only blur masks")
	@click.argument("sequences", nargs=-1)
	@with_appcontext
	def cleanup(sequences, full, database, cache, blur):
		"""Cleans up GeoVisio files and database."""
		if full is False and database is False and cache is False and blur is False:
			full = True
		runner_pictures.cleanup(sequences, full, database, cache, blur)

	@app.cli.command("fill-mock-data")
	@click.option("--base-image-path", required=True, help="local path to a base image that will be duplicated")
	@click.option("--nb-sequences", default=100, show_default=True, help="number of sequence to generate")
	@click.option("--nb-pictures-per-sequence", default=100, show_default=True, help="number max of photo per sequence")
	@click.option("--max-size", default="50MB", show_default=True, help="Maximum size of pictures to generate. The size must have a unit: B, KB, MB or GB")
	@with_appcontext
	def fill_with_mock_data(base_image_path, nb_sequences, nb_pictures_per_sequence, max_size):
		"""Creates mock sequences and images for testing purposes."""
		from .scripts import mock_data
		return mock_data.fill_with_mock_data(base_image_path, nb_sequences, nb_pictures_per_sequence, max_size)

	@app.cli.group("db")
	def db():
		"""Commands to handle database operations"""
		pass

	@db.command("upgrade")
	@with_appcontext
	def upgrade():
		"""Update database schema"""
		db_migrations.update_db_schema(app.config['DB_URL'], force=True)

	@db.command("rollback")
	@click.option('--all', is_flag=True, default=False, show_default=True, help="rollbacks all migrations instead, meaning everything created by Geovisio in database is deleted")
	@with_appcontext
	def rollback(all):
		"""Rollbacks the latest database migration"""
		db_migrations.rollback_db_schema(app.config['DB_URL'], all)

	return app



def createDirNoFailure(directory):
	"""Creates a directory on disk if not already existing

	Parameters
	----------
	directory : str
		Path of the directory to create
	"""

	try:
		os.makedirs(directory)
	except OSError:
		pass

def _get_threads_limit(param):
	if param == 0:
		return os.cpu_count()
	return min(int(param), os.cpu_count())
