import flask
from flask import current_app, url_for, session, redirect, request
import psycopg
from functools import wraps
from authlib.integrations.flask_client import OAuth
from dataclasses import dataclass
import json
from abc import ABC, abstractmethod
from typing import Any

bp = flask.Blueprint("auth", __name__, url_prefix="/api/auth")

ACCOUNT_KEY = "account"  # Key in flask's session with the account's information
NEXT_URL_KEY = (
	"next-url"  # Key in flask's session with url to be redirected after the oauth dance
)

oauth = OAuth()
oauth_provider = None


@dataclass
class OAuthUserAccount(object):
	id: str
	name: str


class OAuthProvider(ABC):
	"""Base class for oauth provider. Need so specify how to get user's info"""

	name: str
	client: Any

	def __init__(self, name, **kwargs) -> None:
		super(OAuthProvider, self).__init__()
		self.name = name
		self.client = oauth.register(name=name, **kwargs)

	@abstractmethod
	def get_user_oauth_info(self, tokenResponse) -> OAuthUserAccount:
		pass


class OIDCProvider(OAuthProvider):
	def __init__(self, *args, **kwargs) -> None:
		super(OIDCProvider, self).__init__(*args, **kwargs)

	def get_user_oauth_info(self, tokenResponse) -> OAuthUserAccount:
		# user info is alway provided by oidc provider, nothing to do
		# we only need the 'sub' (subject) claim
		oidc_userinfo = tokenResponse["userinfo"]
		return OAuthUserAccount(id=oidc_userinfo["sub"], name=oidc_userinfo["name"])


class KeycloakProvider(OIDCProvider):
	def __init__(self, keycloack_realm_user, client_id, client_secret) -> None:
		super().__init__(
			name="keycloak",
			client_id=client_id,
			client_secret=client_secret,
			server_metadata_url=f"{keycloack_realm_user}/.well-known/openid-configuration",
			client_kwargs={
				"scope": "openid",
				"code_challenge_method": "S256",  # enable PKCE
			},
		)


class OSMOAuthProvider(OAuthProvider):
	def __init__(self, oauth_key, oauth_secret) -> None:
		super().__init__(
			name="osm",
			client_id=oauth_key,
			client_secret=oauth_secret,
			api_base_url="https://api.openstreetmap.org/api/0.6/",
			authorize_url="https://www.openstreetmap.org/oauth2/authorize",
			access_tokenurl="https://www.openstreetmap.org/oauth2/token",
			client_kwargs={
				"scope": "read_prefs",
			},
		)

	def get_user_oauth_info(self, tokenResponse) -> OAuthUserAccount:
		"""Get the id/name of the logged user from osm's API
		cf. https://wiki.openstreetmap.org/wiki/API_v0.6
		Args:
				tokenResponse: access token to the OSM api, will be automatically used to query the OSM API

		Returns:
				OAuthUserAccount: id and name of the account
		"""
		details = self.client.get("user/details.json")
		details.raise_for_status()
		details = details.json()
		return OAuthUserAccount(
			id=str(details["user"]["id"]), name=details["user"]["display_name"]
		)


def make_auth(app):
	def ensure(*app_config_key):
		missing = [k for k in app_config_key if k not in app.config]
		if missing:
			raise Exception(
				f"To setup a keycloak as identity provider, you need to provide {missing} in configuration"
			)

	global oauth_provider
	if app.config.get("OAUTH_PROVIDER") == "oidc":
		ensure("OIDC_URL", "CLIENT_ID", "CLIENT_SECRET")

		oauth_provider = KeycloakProvider(
			app.config["OIDC_URL"],
			app.config["CLIENT_ID"],
			app.config["CLIENT_SECRET"],
		)
	elif app.config.get("OAUTH_PROVIDER") == "osm":
		ensure("CLIENT_ID", "CLIENT_SECRET")

		oauth_provider = OSMOAuthProvider(
			app.config["CLIENT_ID"],
			app.config["CLIENT_SECRET"],
		)
	else:
		raise Exception(
			"Unsupported OAUTH_PROVIDER, should be either 'oidc' or 'osm'. If you want another provider to be supported, add a subclass to OAuthProvider"
		)
	oauth.init_app(app)

	return oauth


@bp.route("/login")
def login():
	"""Log in geovisio
 
	Will log in the provided identity provider
	---
	responses:
		302:
			description: Identity provider login page
	"""

	next_url = request.args.get("next_url")
	if next_url:
		# we store the next_url in the session, to be able to redirect the users to this url after the oauth dance
		session[NEXT_URL_KEY] = next_url
	return oauth_provider.client.authorize_redirect(
		url_for("auth.auth", _external=True, _scheme=request.scheme)
	)


@dataclass
class Account(object):
	id: str
	name: str
	oauth_provider: str
	oauth_id: str


@bp.route("/redirect")
def auth():
	"""Redirect endpoint after log in the identity provider
 
	This endpoint should be called by the identity provider after a sucessful login
	---
	responses:
		200:
			description: Information about the logged account
			schema:
				type: object
				properties:
				  	id:
				  		type: string
				  		format: uuid
				  	name:
				  		type: string
				  	oauth_provider:
				  		type: string
				  	oauth_id:
				  		type: string
			headers: 
				Set-Cookie:
					description: 2 cookies are set: `user_id` and `user-name`
					schema:
						type: string
	"""
	tokenResponse = oauth_provider.client.authorize_access_token()

	oauth_info = oauth_provider.get_user_oauth_info(tokenResponse)
	with psycopg.connect(current_app.config["DB_URL"]) as conn:
		with conn.cursor() as cursor:
			res = cursor.execute(
				"INSERT INTO accounts (name, oauth_provider, oauth_id) VALUES (%(name)s, %(provider)s, %(id)s) ON CONFLICT (oauth_provider, oauth_id) DO UPDATE SET name = %(name)s RETURNING id, name",
				{
					"provider": oauth_provider.name,
					"id": oauth_info.id,
					"name": oauth_info.name,
				},
			).fetchone()
			if res is None:
				raise Exception("Impossible to insert user in database")
			id, name = res
			account = Account(
				id=str(id),  # convert uuid to string for serialization
				name=name,
				oauth_provider=oauth_provider.name,
				oauth_id=oauth_info.id,
			)
			session[ACCOUNT_KEY] = account.__dict__
   
			next_url = session.pop(NEXT_URL_KEY, None)
			if next_url:
				response = flask.make_response(redirect(next_url))
			else:
				response = flask.make_response(redirect("/"))

			# also store id/name in cookies for the front end to use those
			response.set_cookie("user_id", str(id), max_age=current_app.config['PERMANENT_SESSION_LIFETIME'])
			response.set_cookie("user_name", name, max_age=current_app.config['PERMANENT_SESSION_LIFETIME'])

			return response



def login_required(mandatory_login_param):
	"""Check that the user is logged, and abort if it's not the case
 
	Args:
		mandatory_login_param (str): name of the configuration parameter used to decide if the login is mandatory or not
	"""
	def actual_decorator(f):
		@wraps(f)
		def decorator(*args, **kwargs):
			account = get_current_account()
			if not account and current_app.config[mandatory_login_param]:
				return flask.abort(flask.make_response(flask.jsonify(message="Authentication is mandatory"), 401))
			kwargs["account"] = account

			return f(*args, **kwargs)

		return decorator
	return actual_decorator

def login_required_with_redirect():
	"""Check that the user is logged, and redirect if it's not the case
	"""
	def actual_decorator(f):
		@wraps(f)
		def decorator(*args, **kwargs):
			account = get_current_account()
			if not account:
				if 'OAUTH_PROVIDER' not in current_app.config:
					return flask.abort(flask.make_response(flask.jsonify(message="Authentication has not been activated in this instance, impossible to log in."), 403))
				return redirect(url_for("auth.login", next_url=request.url))
			kwargs["account"] = account

			return f(*args, **kwargs)

		return decorator
	return actual_decorator

class UnknowAccountException(Exception):
	status_code = 401

	def __init__(self):
		msg = f"No account with this oauth id is know, you should login first"
		super().__init__(msg)


class LoginRequiredException(Exception):
	status_code = 401

	def __init__(self):
		msg = f"You should login to request this API"
		super().__init__(msg)


def get_current_account():
	"""Get the authenticated account information from the flask session

	Returns:
			Account: the current logged account, None if nobody is logged
	"""
	if ACCOUNT_KEY in session:
		session_account = Account(**session[ACCOUNT_KEY])

		return session_account

	return None


@bp.route("/logout")
def logout():
	"""Log out from geovisio
	---
	responses:
		302:
			description: Home page redirection
	"""
	# TODO should we also logout from Identity Provider?
	session.pop(ACCOUNT_KEY, None)
 
	r = flask.make_response(redirect("/"))
	# also unset id/name in cookies
	r.set_cookie("user_id", "", max_age=0)
	r.set_cookie("user_name", "", max_age=0)

	return r
