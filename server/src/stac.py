import psycopg
import re
import json
from uuid import UUID
from psycopg.errors import UniqueViolation
from psycopg.rows import dict_row
from psycopg.types.json import Jsonb
from dateutil import tz
from dateutil.parser import parse as dateparser
from flask import Blueprint, current_app, request, url_for
from flask_executor import Executor
from urllib.parse import unquote
from werkzeug.datastructures import MultiDict
from PIL import Image
from fs import open_fs
from . import errors, pictures, runner_pictures, auth


STAC_VERSION = "1.0.0-rc.2"
STAC_PREFIX = "/api"
CONFORMANCE_LIST = [
	"http://www.opengis.net/spec/ogcapi-features-1/1.0/conf/core",
	"http://www.opengis.net/spec/ogcapi-features-1/1.0/conf/geojson",
	f"https://api.stacspec.org/v{STAC_VERSION}/core",
	f"https://api.stacspec.org/v{STAC_VERSION}/browseable",
	f"https://api.stacspec.org/v{STAC_VERSION}/children",
	f"https://api.stacspec.org/v{STAC_VERSION}/collections",
	f"https://api.stacspec.org/v{STAC_VERSION}/ogcapi-features",
	f"https://api.stacspec.org/v{STAC_VERSION}/item-search",
]

executor = Executor()
bp = Blueprint('stac', __name__, url_prefix=STAC_PREFIX)


@bp.route('/')
def getLanding():
	"""Retrieves API resources list
	---
		responses:
			200:
				description: the Catalog listing resources available in this API. A non-standard "extent" property is also available (note that this may evolve in the future)
				schema:
					allOf:
						- $ref: 'https://schemas.stacspec.org/v1.0.0/catalog-spec/json-schema/catalog.json'
						- type: object
						  properties:
							extent:
								$ref: '#/definitions/extent'
	"""

	with psycopg.connect(current_app.config['DB_URL']) as conn:
		with conn.cursor() as cursor:
			spatial_xmin, spatial_ymin, spatial_xmax, spatial_ymax, temporal_min, temporal_max = cursor.execute("""
				SELECT
					ST_XMin(ST_EstimatedExtent('pictures', 'geom')),
					ST_YMin(ST_EstimatedExtent('pictures', 'geom')),
					ST_XMax(ST_EstimatedExtent('pictures', 'geom')),
					ST_YMax(ST_EstimatedExtent('pictures', 'geom')),
					MIN(ts), MAX(ts)
				FROM pictures
			""").fetchone()

			extent = {
				"spatial": { "bbox": [[ spatial_xmin, spatial_ymin, spatial_xmax, spatial_ymax ]] } if spatial_xmin is not None else None,
				"temporal": { "interval": [[ temporal_min.isoformat(), temporal_max.isoformat() ]] } if temporal_min is not None else None
			} if spatial_xmin is not None or temporal_min is not None else None

			sequences = [
				{
					"rel": "child",
					"title": f"User \"{s[1]}\" sequences",
					"href": url_for('stac.getUserCatalog', userId=s[0], _external=True)
				}
				for s in cursor.execute("""
					SELECT DISTINCT s.account_id, a.name
					FROM sequences s
					JOIN accounts a ON s.account_id = a.id
				""").fetchall()
			]

			catalog = dbSequencesToStacCatalog(
				"geovisio",
				"GeoVisio STAC API",
				"This catalog list all geolocated pictures available in this GeoVisio instance",
				sequences, request, extent
			)

			catalog["links"] += [
				{
					"rel": "service-desc",
					"type": "application/json",
					"href": url_for("flasgger.apispec_1", _external=True)
				},
				{
					"rel": "service-doc",
					"type": "text/html",
					"href": url_for("apidocsNoSlash", _external=True)
				},
				{
					"rel": "conformance",
					"type": "application/json",
					"href": url_for("stac.getConformance", _external=True)
				},
				{
					"rel": "data",
					"type": "application/json",
					"href": url_for("stac.getAllCollections", _external=True)
				},
				{
					"rel": "search",
					"type": "application/geo+json",
					"href": url_for("stac.searchItems", _external=True)
				}
			]

			return catalog, 200, {"Content-Type": "application/json" }

		raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)

	raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)


@bp.route('/conformance')
def getConformance():
	"""List definitions this API conforms to
	---
		responses:
			200:
				description: the list of definitions this API conforms to
	"""

	return { "conformsTo": CONFORMANCE_LIST }, 200, {"Content-Type": "application/json" }


def dbSequenceToStacCollection(dbSeq):
	"""Transforms a sequence extracted from database into a STAC Collection

	Parameters
	----------
	dbSeq : dict
		A row from sequences table in database (with id, name, minx, miny, maxx, maxy, mints, maxts fields)
	request
	current_app

	Returns
	-------
	object
		The equivalent in STAC Collection format
	"""

	mints, maxts = dbSeq.get("mints"), dbSeq.get("maxts")
	return cleanNoneInDict({
		"type": "Collection",
		"stac_version": STAC_VERSION,
		"id": str(dbSeq["id"]),
		"title": str(dbSeq["name"]),
		"description": "A sequence of geolocated pictures",
		"keywords": ["pictures", str(dbSeq["name"])],
		"license": "proprietary", # TODO Handle a proper licensing system
		"providers": [
			{"name": dbSeq["account_name"], "roles": ["producer"]},
        ],
		"extent": {
			"spatial": { "bbox": [ [dbSeq["minx"] or -180.0, dbSeq["miny"] or -90.0, dbSeq["maxx"] or 180.0, dbSeq["maxy"] or 90.0] ] },
			"temporal": { "interval": [[
				mints.astimezone(tz.gettz('UTC')).isoformat() if mints is not None else None,
				maxts.astimezone(tz.gettz('UTC')).isoformat() if maxts is not None else None,
		]] }
		},
		"summaries": cleanNoneInDict({
			"pers:interior_orientation": dbSeq.get("metas")
		}, noneIfEmpty=False),
		"links": [
			{
			  "rel": "items",
			  "type": "application/geo+json",
			  "title": "Pictures in this sequence",
			  "href": url_for("stac.getCollectionItems", _external=True, collectionId=dbSeq["id"])
			},
			{
			  "rel": "parent",
			  "type": "application/json",
			  "title": "Instance catalog",
			  "href": url_for('stac.getLanding', _external=True)
			},
			{
			  "rel": "root",
			  "type": "application/json",
			  "title": "Instance catalog",
			  "href": url_for('stac.getLanding', _external=True)
			},
			{
			  "rel": "self",
			  "type": "application/json",
			  "title": "Metadata of this sequence",
			  "href": url_for("stac.getCollection", _external=True, collectionId=dbSeq["id"])
			}
		]
	})


def dbSequencesToStacCatalog(id, title, description, sequences, request, extent = None, **selfUrlValues):
	"""Transforms a set of sequences into a STAC Catalog

	Parameters
	----------
	id : str
		The catalog ID
	title : str
		The catalog name
	description : str
		The catalog description
	sequences : list
		List of sequences as STAC child links
	request
	current_app
	extent : dict
		Spatial and temporal extent of the catalog, in STAC format
	selfRoute : str
		API route to access this catalog (defaults to empty, for root catalog)

	Returns
	-------
	object
		The equivalent in STAC Catalog format
	"""

	return cleanNoneInDict({
		"stac_version": STAC_VERSION,
		"id": id,
		"title": title,
		"description": description,
		"type": "Catalog",
		"conformsTo" : CONFORMANCE_LIST,
		"extent": extent,
		"links": [
			{
				"rel": "self",
				"type": "application/json",
				"href": url_for(request.endpoint, _external=True, **selfUrlValues)
			},
			{
				"rel": "root",
				"type": "application/json",
				"href": url_for('stac.getLanding', _external=True)
			}
		] + sequences
	})


@bp.route('/collections')
def getAllCollections():
	"""List available collections
	---
		responses:
			200:
				description: the list of available collections
				schema:
					$ref: '#/definitions/Collections'
	"""

	with psycopg.connect(current_app.config['DB_URL'], row_factory=dict_row) as conn:
		with conn.cursor() as cursor:
			records = cursor.execute("""
				SELECT
					s.id, COALESCE(s.metadata->>'title', s.folder_path) AS name,
					ST_XMin(s.geom) AS minx,
					ST_YMin(s.geom) AS miny,
					ST_XMax(s.geom) AS maxx,
					ST_YMax(s.geom) AS maxy,
					accounts.name AS account_name,
					a.mints,
					a.maxts
				FROM sequences s
				JOIN accounts ON accounts.id = s.account_id
				JOIN (
					SELECT
						sp.seq_id,
						MIN(ts) as mints,
						MAX(ts) as maxts
					FROM pictures p
					JOIN sequences_pictures sp ON sp.pic_id = p.id
					GROUP BY sp.seq_id
				) a ON a.seq_id = s.id
			""")

			collections = [ dbSequenceToStacCollection(dbSeq) for dbSeq in records ]

			return {
				"collections": collections,
				"links": [
					{
					  "rel": "root",
					  "type": "application/json",
					  "href": url_for('stac.getLanding', _external=True)
					},
					{
					  "rel": "parent",
					  "type": "application/json",
					  "href": url_for('stac.getLanding', _external=True)
					},
					{
					  "rel": "self",
					  "type": "application/json",
					  "href": url_for("stac.getAllCollections", _external=True)
					}
				]
			}, 200, {"Content-Type": "application/json" }

		raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)

	raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)


@bp.route('/collections/<uuid:collectionId>')
def getCollection(collectionId):
	"""Retrieve metadata of a single collection
	---
		parameters:
			- name: collectionId
			  in: path
			  type: string
			  description: ID of collection to retrieve
		responses:
			200:
				description: the collection metadata
				schema:
					$ref: 'https://schemas.stacspec.org/v1.0.0/collection-spec/json-schema/collection.json'
	"""

	with psycopg.connect(current_app.config['DB_URL'], row_factory=dict_row) as conn:
		with conn.cursor() as cursor:
			record = cursor.execute("""
				SELECT
					s.id, COALESCE(s.metadata->>'title', s.folder_path) AS name,
					ST_XMin(s.geom) AS minx,
					ST_YMin(s.geom) AS miny,
					ST_XMax(s.geom) AS maxx,
					ST_YMax(s.geom) AS maxy,
     				accounts.name AS account_name,
					a.*
				FROM sequences s
				JOIN accounts ON s.account_id = accounts.id, (
					SELECT
						MIN(ts) as mints,
						MAX(ts) as maxts,
						array_agg(DISTINCT jsonb_build_object(
							'make', metadata->>'make',
							'model', metadata->>'model',
							'focal_length', metadata->>'focal_length',
							'field_of_view', metadata->>'field_of_view'
						)) AS metas
					FROM pictures p
					JOIN sequences_pictures sp ON sp.seq_id = %(id)s AND sp.pic_id = p.id
				) a
				WHERE s.id = %(id)s
			""", { "id": collectionId }).fetchone()

			if record is None:
				raise errors.InvalidAPIUsage("Collection doesn't exist", status_code=404)

			return dbSequenceToStacCollection(record), 200, {
				"Content-Type": "application/json",
			}

		raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)

	raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)


@bp.route('/users/<uuid:userId>/catalog/')
def getUserCatalog(userId):
	"""Retrieves an user list of sequences (catalog)
	---
		responses:
			200:
				description: the Catalog listing all sequences associated to given user
				schema:
					$ref: 'https://schemas.stacspec.org/v1.0.0/catalog-spec/json-schema/catalog.json'
	"""

	with psycopg.connect(current_app.config['DB_URL']) as conn:
		with conn.cursor() as cursor:
			userName = cursor.execute("SELECT name FROM accounts WHERE id = %s", [ userId ]).fetchone()[0]
			sequences = [
				{
					"rel": "child",
			  		"href": url_for("stac.getCollection", _external=True, collectionId=s[0])
				}
				for s in cursor.execute("SELECT id FROM sequences WHERE account_id = %s", [ userId ]).fetchall()
			]

			return dbSequencesToStacCatalog(
				f"user:{userId}",
				f"{userName}'s sequences",
				f"List of all sequences of user {userName}",
				sequences, request,
				userId=str(userId)
			), 200, {"Content-Type": "application/json" }

		raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)

	raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)


def cleanNoneInDict(val, noneIfEmpty=True):
	"""Removes empty values from dictionnary"""
	res = {k: v for k, v in val.items() if v is not None}
	return res if len(res) > 0 or not noneIfEmpty else None


def dbPictureToStacItem(seqId, dbPic, request, current_app):
	"""Transforms a picture extracted from database into a STAC Item

	Parameters
	----------
	seqId : uuid
		Associated sequence ID
	dbSeq : dict
		A row from pictures table in database (with id, geojson, ts, heading, cols, rows, width, height, prevpic, nextpic, prevpicgeojson, nextpicgeojson fields)
	request
	current_app

	Returns
	-------
	object
		The equivalent in STAC Item format
	"""

	item = {
		"type": "Feature",
		"stac_version": STAC_VERSION,
		"stac_extensions": [
			"https://stac-extensions.github.io/view/v1.0.0/schema.json", # "view:" fields
			"https://raw.githubusercontent.com/stac-extensions/perspective-imagery/main/json-schema/schema.json" # "pers:" fields
		],
		"id": str(dbPic["id"]),
		"geometry": dbPic["geojson"],
		"bbox": dbPic["geojson"]["coordinates"] + dbPic["geojson"]["coordinates"],
		"providers": [
      		{"name": dbPic["account_name"], "roles": ["producer"]},
        ],
		"properties": {
			"datetime": dbPic["ts"].astimezone(tz.gettz('UTC')).isoformat(),
			"license": "proprietary", # TODO Proper handling of licenses
			"view:azimuth": dbPic["heading"],
			"pers:interior_orientation": cleanNoneInDict({
				"camera_manufacturer": dbPic["metadata"].get("make"),
				"camera_model": dbPic["metadata"].get("model"),
				"focal_length": dbPic["metadata"].get("focal_length"),
				"field_of_view": dbPic["metadata"].get("field_of_view")
			}) if "metadata" in dbPic and len([ True for f in dbPic["metadata"] if f in ["make", "model", "focal_length", "field_of_view"]]) > 0 else {}
		},
		"links": [
			{
				"rel": "root",
				"type": "application/json",
				"href": url_for("stac.getLanding")
			},
			{
				"rel": "parent",
				"type": "application/json",
			  	"href": url_for("stac.getCollection", _external=True, collectionId=seqId)
			},
			{
				"rel": "self",
				"type": "application/geo+json",
			  	"href": url_for("stac.getCollectionItem", _external=True, collectionId=seqId, itemId=dbPic["id"])
			},
			{
				"rel": "collection",
				"type": "application/json",
			  	"href": url_for("stac.getCollection", _external=True, collectionId=seqId)
			}
		],
		"assets": {
			"hd_webp": {
				"title": "HD picture",
				"description": "Highest resolution available of this picture",
				"roles": [ "data" ],
				"type": "image/webp",
			  	"href": url_for("pictures.getPictureHD", _external=True, pictureId=dbPic["id"], format="webp")
			},
			"sd_webp": {
				"title": "SD picture",
				"description": "Picture in standard definition (fixed width of 2048px)",
				"roles": [ "visual" ],
				"type": "image/webp",
			  	"href": url_for("pictures.getPictureSD", _external=True, pictureId=dbPic["id"], format="webp")
			},
			"thumb_webp": {
				"title": "Thumbnail",
				"description": "Picture in low definition (fixed width of 500px)",
				"roles": [ "thumbnail" ],
				"type": "image/webp",
			  	"href": url_for("pictures.getPictureThumb", _external=True, pictureId=dbPic["id"], format="webp")
			},
			"hd": {
				"title": "HD picture",
				"description": "Highest resolution available of this picture",
				"roles": [ "data" ],
				"type": "image/jpeg",
			  	"href": url_for("pictures.getPictureHD", _external=True, pictureId=dbPic["id"], format="jpg")
			},
			"sd": {
				"title": "SD picture",
				"description": "Picture in standard definition (fixed width of 2048px)",
				"roles": [ "visual" ],
				"type": "image/jpeg",
			  	"href": url_for("pictures.getPictureSD", _external=True, pictureId=dbPic["id"], format="jpg")
			},
			"thumb": {
				"title": "Thumbnail",
				"description": "Picture in low definition (fixed width of 500px)",
				"roles": [ "thumbnail" ],
				"type": "image/jpeg",
			  	"href": url_for("pictures.getPictureThumb", _external=True, pictureId=dbPic["id"], format="jpg")
			}
		},
		"collection": str(seqId)
	}

	# Next / previous links if any
	if "nextpic" in dbPic and dbPic["nextpic"] is not None:
		item["links"].append({
			"rel": "next",
			"type": "application/geo+json",
			"geometry": dbPic["nextpicgeojson"],
			"id": dbPic["nextpic"],
			"href": url_for("stac.getCollectionItem", _external=True, collectionId=seqId, itemId=dbPic["nextpic"])
		})

	if "prevpic" in dbPic and dbPic["prevpic"] is not None:
		item["links"].append({
			"rel": "prev",
			"type": "application/geo+json",
			"geometry": dbPic["prevpicgeojson"],
			"id": dbPic["prevpic"],
			"href": url_for("stac.getCollectionItem", _external=True, collectionId=seqId, itemId=dbPic["prevpic"])
		})

	#
	# Picture type-specific properties
	#

	# Equirectangular
	if dbPic["metadata"]["type"] == "equirectangular":
		item["stac_extensions"].append("https://stac-extensions.github.io/tiled-assets/v1.0.0/schema.json") # "tiles:" fields

		item["properties"]["tiles:tile_matrix_sets"] = {
			"geovisio": {
				"type": "TileMatrixSetType",
				"title": "GeoVisio tile matrix for picture " + str(dbPic["id"]),
				"identifier": "geovisio-" + str(dbPic["id"]),
				"tileMatrix": [
					{
						"type": "TileMatrixType",
						"identifier": "0",
						"scaleDenominator": 1,
						"topLeftCorner": [ 0,0 ],
						"tileWidth": dbPic["metadata"]["width"] / dbPic["metadata"]["cols"],
						"tileHeight": dbPic["metadata"]["height"] / dbPic["metadata"]["rows"],
						"matrixWidth": dbPic["metadata"]["cols"],
						"matrixHeight": dbPic["metadata"]["rows"]
					}
				]
			}
		}

		item["asset_templates"] = {
			"tiles_webp": {
				"title": "HD tiled picture",
				"description": "Highest resolution available of this picture, as tiles",
				"roles": [ "data" ],
				"type": "image/webp",
				"href": unquote(url_for("pictures.getPictureTile", _external=True, pictureId=dbPic["id"], format="webp", col="{TileCol}", row="{TileRow}"))
			},
			"tiles": {
				"title": "HD tiled picture",
				"description": "Highest resolution available of this picture, as tiles",
				"roles": [ "data" ],
				"type": "image/jpeg",
				"href": unquote(url_for("pictures.getPictureTile", _external=True, pictureId=dbPic["id"], format="jpg", col="{TileCol}", row="{TileRow}"))
			}
		}

	return item


@bp.route('/collections/<uuid:collectionId>/items', methods=["GET"])
def getCollectionItems(collectionId):
	"""List items of a single collection
	---
		parameters:
			- name: collectionId
			  in: path
			  type: string
			  description: ID of collection to retrieve
		responses:
			200:
				description: the items list
				schema:
					$ref: 'https://geojson.org/schema/FeatureCollection.json'
	"""

	with psycopg.connect(current_app.config['DB_URL'], row_factory=dict_row) as conn:
		with conn.cursor() as cursor:
			records = cursor.execute("""
				SELECT
					p.id, p.ts, p.heading, p.metadata,
					ST_AsGeoJSON(p.geom)::json AS geojson,
					a.name AS account_name,
					CASE WHEN LAG(p.status) OVER othpics = 'ready' THEN LAG(p.id) OVER othpics END AS prevpic,
					CASE WHEN LAG(p.status) OVER othpics = 'ready' THEN ST_AsGeoJSON(LAG(p.geom) OVER othpics)::json END AS prevpicgeojson,
					CASE WHEN LEAD(p.status) OVER othpics = 'ready' THEN LEAD(p.id) OVER othpics END AS nextpic,
					CASE WHEN LEAD(p.status) OVER othpics = 'ready' THEN ST_AsGeoJSON(LEAD(p.geom) OVER othpics)::json END AS nextpicgeojson
				FROM sequences_pictures sp
				JOIN pictures p ON sp.pic_id = p.id
				JOIN accounts a ON a.id = p.account_id
				WHERE sp.seq_id = %(seq)s AND p.status = 'ready'
				WINDOW othpics AS (PARTITION BY sp.seq_id ORDER BY sp.rank)
				ORDER BY rank
			""", { "seq": collectionId })

			items = [ dbPictureToStacItem(
				collectionId,
				dbPic,
				request,
				current_app
			) for dbPic in records ]

			return {
				"type": "FeatureCollection",
				"features": items,
				"links": [
					{
						"rel": "root",
						"type": "application/json",
						"href": url_for("stac.getLanding", _external=True)
					},
					{
						"rel": "parent",
						"type": "application/json",
			  			"href": url_for("stac.getCollection", _external=True, collectionId=collectionId)
					},
					{
						"rel": "self",
						"type": "application/geo+json",
			  			"href": url_for("stac.getCollectionItems", _external=True, collectionId=collectionId)
					}
				]
			}, 200, {"Content-Type": "application/geo+json" }

		raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)

	raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)


@bp.route('/collections/<uuid:collectionId>/items/<uuid:itemId>')
def getCollectionItem(collectionId, itemId):
	"""Get a single item from a collection
	---
		parameters:
			- name: collectionId
			  in: path
			  type: string
			  description: ID of collection to retrieve
			- name: itemId
			  in: path
			  type: string
			  description: ID of item to retrieve
		responses:
			102:
				description: the item (which is still under process)
				schema:
					$ref: 'https://schemas.stacspec.org/v1.0.0/item-spec/json-schema/item.json'
			200:
				description: the wanted item
				schema:
					$ref: 'https://schemas.stacspec.org/v1.0.0/item-spec/json-schema/item.json'
	"""

	with psycopg.connect(current_app.config['DB_URL'], row_factory=dict_row) as conn:
		with conn.cursor() as cursor:
			# Get rank + position of wanted picture
			record = cursor.execute("""
				SELECT p.id, sp.rank, ST_AsGeoJSON(p.geom)::json AS geojson, p.heading, p.ts, p.metadata, p.status, accounts.name AS account_name
				FROM sequences_pictures sp, pictures p
				JOIN accounts ON p.account_id = accounts.id
				WHERE sp.seq_id = %(seq)s
					AND sp.pic_id = %(pic)s
					AND p.id = %(pic)s
					AND p.status != 'hidden'
			""", { "seq": collectionId, "pic": itemId }).fetchone()

			if record is None:
				raise errors.InvalidAPIUsage("Item doesn't exist", status_code=404)

			# Get next/prev pic in sequence position and ID
			for rank in [record["rank"]-1, record["rank"]+1]:
				record2 = cursor.execute("""
					SELECT sp.pic_id AS id, ST_AsGeoJSON(p.geom)::json AS geojson
					FROM sequences_pictures sp
					JOIN pictures p on sp.pic_id = p.id
					WHERE sp.seq_id = %(seq)s
						AND sp.rank = %(rank)s
						AND p.status = 'ready'
				""", { "seq": collectionId, "rank": rank }).fetchone()

				if record2:
					if rank == record["rank"]-1:
						record["prevpic"] = record2["id"]
						record["prevpicgeojson"] = record2["geojson"]
					elif rank == record["rank"]+1:
						record["nextpic"] = record2["id"]
						record["nextpicgeojson"] = record2["geojson"]

			picStatusToHttpCode = { "preparing": 102, "ready": 200, "broken": 500 }
			return dbPictureToStacItem(
				collectionId,
				record,
				request,
				current_app
			), picStatusToHttpCode[record["status"]], {"Content-Type": "application/geo+json" }

		raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)

	raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)


@bp.route('/search', methods=['GET', 'POST'])
def searchItems():
	"""Search through all available items
	---
		parameters:
			- name: limit
			  in: query, body
			  type: number
			  description: The maximum number of results to return
			- name: bbox
			  in: query, body
			  type: [number]
			  description: Requested bounding box
			- name: datetime
			  in: query, body
			  type: string
			  description: Single date+time, or a range ('/' separator), formatted to RFC 3339, section 5.6. Use double dots .. for open date ranges.
			- name: intersects
			  in: query, body
			  type: string
			  description: Searches items by performing intersection between their geometry and provided GeoJSON geometry
			- name: ids
			  in: query, body
			  type: [string]
			  description: Array of Item ids to return
			- name: collections
			  in: query, body
			  type: [string]
			  description: Array of one or more Collection IDs that each matching Item must be in
		responses:
			200:
				description: the items list
				schema:
					$ref: 'https://geojson.org/schema/FeatureCollection.json'
	"""

	sqlWhere = [ "p.status = 'ready'" ]
	sqlParams = {}

	#
	# Parameters parsing and verification
	#

	# Method + content-type
	args = None
	if request.method == 'GET':
		args = request.args
	elif request.method == 'POST':
		if request.headers.get('Content-Type') == 'application/json':
			args = MultiDict(request.json)
		else:
			raise errors.InvalidAPIUsage("Search using POST method should have a JSON body", status_code=400)

	# Limit
	if args.get("limit") is not None:
		limit = args.get("limit", type=int)
		if limit is None or limit < 1 or limit > 10000:
			raise errors.InvalidAPIUsage("Parameter limit must be either empty or a number between 1 and 10000", status_code=400)
		else:
			sqlParams["limit"] = limit
	else:
		sqlParams["limit"] = 10000

	# Bounding box
	if args.get("bbox") is not None:
		try:
			bbox = [float(n) for n in args.get("bbox")[1:-1].split(",")]
			if len(bbox) != 4 or not all(isinstance(x, float) for x in bbox):
				raise ValueError()
			elif bbox[0] < -180 or bbox[0] > 180 or bbox[1] < -90 or bbox[1] > 90 or bbox[2] < -180 or bbox[2] > 180 or bbox[3] < -90 or bbox[3] > 90:
				raise errors.InvalidAPIUsage("Parameter bbox must contain valid longitude (-180 to 180) and latitude (-90 to 90) values", status_code=400)
			else:
				sqlWhere.append("p.geom && ST_MakeEnvelope(%(minx)s, %(miny)s, %(maxx)s, %(maxy)s, 4326)")
				sqlParams["minx"] = bbox[0]
				sqlParams["miny"] = bbox[1]
				sqlParams["maxx"] = bbox[2]
				sqlParams["maxy"] = bbox[3]
		except ValueError:
			raise errors.InvalidAPIUsage("Parameter bbox must be in format [minX, minY, maxX, maxY]", status_code=400)

	# Datetime
	if args.get("datetime") is not None:
		try:
			dates = args.get("datetime").split("/")

			if len(dates) == 1:
				date = dateparser(dates[0])
				sqlWhere.append("p.ts = %(ts)s::timestamp with time zone")
				sqlParams["ts"] = date

			elif len(dates) == 2:
				# Check if interval is closed or open-ended
				if dates[0] == "..":
					date = dateparser(dates[1])
					sqlWhere.append("p.ts <= %(ts)s::timestamp with time zone")
					sqlParams["ts"] = date
				elif dates[1] == "..":
					date = dateparser(dates[0])
					sqlWhere.append("p.ts >= %(ts)s::timestamp with time zone")
					sqlParams["ts"] = date
				else:
					date0 = dateparser(dates[0])
					date1 = dateparser(dates[1])
					sqlWhere.append("p.ts >= %(mints)s::timestamp with time zone")
					sqlWhere.append("p.ts <= %(maxts)s::timestamp with time zone")
					sqlParams["mints"] = date0
					sqlParams["maxts"] = date1
			else:
				raise errors.InvalidAPIUsage("Parameter datetime should contain one or two dates", status_code=400)
		except:
			raise errors.InvalidAPIUsage("Parameter datetime contains an invalid date definition", status_code=400)

	# Intersects
	if args.get("intersects") is not None:
		try:
			intersects = json.loads(args.get("intersects"))
			if intersects['type'] == "Point":
				sqlWhere.append("ST_DWithin(p.geom::geography, ST_GeomFromGeoJSON(%(geom)s)::geography, 0.01)")
			else:
				sqlWhere.append("p.geom && ST_GeomFromGeoJSON(%(geom)s)")
				sqlWhere.append("ST_Intersects(p.geom, ST_GeomFromGeoJSON(%(geom)s))")
			sqlParams["geom"] = Jsonb(intersects)
		except:
			raise errors.InvalidAPIUsage("Parameter intersects should contain a valid GeoJSON Geometry (not a Feature)", status_code=400)

	# Ids
	if args.get("ids") is not None:
		try:
			sqlWhere.append("p.id = ANY(%(ids)s)")
			sqlParams["ids"] = [ UUID(j) for j in json.loads(args.get("ids")) ]
		except:
			raise errors.InvalidAPIUsage("Parameter ids should be a JSON array of strings", status_code=400)

	# Collections
	if args.get("collections") is not None:
		try:
			sqlWhere.append("sp.seq_id = ANY(%(collections)s)")
			sqlParams["collections"] = [ UUID(j) for j in json.loads(args.get("collections")) ]
		except:
			raise errors.InvalidAPIUsage("Parameter collections should be a JSON array of strings", status_code=400)


	#
	# Database query
	#

	with psycopg.connect(current_app.config['DB_URL'], row_factory=dict_row, options="-c statement_timeout=30000") as conn:
		with conn.cursor() as cursor:
			records = cursor.execute("""
				SELECT
					p.id, p.ts, p.heading, p.metadata,
					ST_AsGeoJSON(p.geom)::json AS geojson,
					sp.seq_id,
					spl.prevpic, spl.prevpicgeojson, spl.nextpic, spl.nextpicgeojson,
					accounts.name AS account_name
				FROM pictures p
				LEFT JOIN sequences_pictures sp ON p.id = sp.pic_id
				LEFT JOIN accounts ON p.account_id = accounts.id
				LEFT JOIN (
					SELECT
						p.id,
						LAG(p.id) OVER othpics AS prevpic,
						ST_AsGeoJSON(LAG(p.geom) OVER othpics)::json AS prevpicgeojson,
						LEAD(p.id) OVER othpics AS nextpic,
						ST_AsGeoJSON(LEAD(p.geom) OVER othpics)::json AS nextpicgeojson
					FROM pictures p
					JOIN sequences_pictures sp ON p.id = sp.pic_id
					WHERE p.status = 'ready'
					WINDOW othpics AS (PARTITION BY sp.seq_id ORDER BY sp.rank)
				) spl ON p.id = spl.id
				WHERE
				""" + " AND ".join(sqlWhere) + """
				LIMIT %(limit)s
			""", sqlParams)

			items = [ dbPictureToStacItem(
				str(dbPic["seq_id"]),
				dbPic,
				request,
				current_app
			) for dbPic in records ]

			return {
				"type": "FeatureCollection",
				"features": items,
				"links": []
			}, 200, {"Content-Type": "application/geo+json" }

		raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)

	raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)


@bp.route('/collections', methods=["POST"])
@auth.login_required("FORCE_AUTH_ON_UPLOAD")
def postCollection(account=None):
	"""Create a new sequence
	---
		parameters:
			- name: title
			  in: body
			  type: string
			  description: The sequence title
		responses:
			200:
				description: the collection metadata
				schema:
					$ref: 'https://schemas.stacspec.org/v1.0.0/collection-spec/json-schema/collection.json'
	"""

	# Parse received parameters
	metadata = {}
	content_type = request.headers.get('Content-Type')
	if content_type == 'application/json':
		metadata["title"] = request.json.get("title")
	elif content_type in ['multipart/form-data', 'application/x-www-form-urlencoded']:
		metadata["title"] = request.form.get("title")

	metadata = cleanNoneInDict(metadata)

	with open_fs(current_app.config['FS_URL']) as fs:
		with psycopg.connect(current_app.config['DB_URL']) as conn:
			with conn.cursor() as cursor:
				# Create sequence folder
				seqFolder = runner_pictures.createSequenceFolder(fs)

				# Get default account ID
				if account is not None:
					accountId = account.id
				else:
					accountId = cursor.execute("SELECT id FROM accounts WHERE is_default").fetchone()[0]

				# Add sequence in database
				seqId = cursor.execute(
					"INSERT INTO sequences(folder_path, status, account_id, metadata) VALUES(%s, 'preparing', %s, %s) RETURNING id",
					[ seqFolder, accountId, Jsonb(metadata) ]
				).fetchone()[0]

				# Make changes definitive in database
				conn.commit()

				# Return created sequence
				return getCollection(seqId)[0], 200, {
					"Content-Type": "application/json",
					"Location": url_for("stac.getCollection", _external=True, collectionId=seqId)
				}

		raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)

	raise errors.InvalidAPIUsage("Failed to access filesystem", status_code=500)


@executor.job
def startPicturePostProcess(sequenceId, sequenceFolder, pictureFilename, associatedAccountID, picId):
	with open_fs(current_app.config['FS_URL']) as fs:
		with psycopg.connect(current_app.config['DB_URL']) as conn:
			picRes = runner_pictures.processNewPicture(fs, conn, sequenceFolder, pictureFilename, associatedAccountID, current_app.config, picId = picId)

			if picRes["status"] == "ready":
				conn.execute("UPDATE sequences SET status = 'ready' WHERE id = %s", [sequenceId])
				conn.commit()


@bp.route('/collections/<uuid:collectionId>/items', methods=["POST"])
@auth.login_required("FORCE_AUTH_ON_UPLOAD")
def postCollectionItem(collectionId, account=None):
	"""Add a new picture in a given sequence
	---
		parameters:
			- name: collectionId
			  in: path
			  type: string
			  description: ID of sequence to add this picture into
			- name: position
			  in: formData
			  type: integer
			  description: Position of picture in sequence (starting from 1)
			- name: picture
			  in: formData
			  type: file
			  description: Picture to upload
		responses:
			202:
				description: the added picture metadata
				schema:
					$ref: 'https://schemas.stacspec.org/v1.0.0/item-spec/json-schema/item.json'
	"""

	if not request.headers.get("Content-Type", "").startswith("multipart/form-data"):
		raise errors.InvalidAPIUsage("Content type should be multipart/form-data", status_code=415)

	# Check if position was given
	if request.form.get("position") is None:
		raise errors.InvalidAPIUsage("Missing \"position\" parameter", status_code=400)
	else:
		try:
			position = int(request.form.get("position"))
			if position <= 0:
				raise ValueError()
		except ValueError:
			raise errors.InvalidAPIUsage("Position in sequence should be a positive integer", status_code=400)

	# Check if a picture file was given
	if "picture" not in request.files:
		raise errors.InvalidAPIUsage("No picture file was sent", status_code=400)
	else:
		picture = request.files["picture"]

		# Check file validity
		if not (picture.filename != "" and "." in picture.filename and picture.filename.rsplit('.', 1)[1].lower() in ["jpg", "jpeg"]):
			raise errors.InvalidAPIUsage("Picture file is either missing or in an unsupported format (should be jpg)", status_code=400)


	with open_fs(current_app.config['FS_URL']) as fs:
		with psycopg.connect(current_app.config['DB_URL']) as conn:
			with conn.cursor() as cursor:
				# Check if sequence exists and find its folder path
				folderPath = cursor.execute("SELECT folder_path FROM sequences WHERE id = %s", [ collectionId ]).fetchone()
				if folderPath is None or len(folderPath) != 1:
					raise errors.InvalidAPIUsage(f"Sequence {collectionId} wasn't found in database", status_code=404)
				folderPath = folderPath[0]

				# Save picture in sequence folder with position in its name
				try:
					picFilename = f"{position:05d}.jpg"
					picFsPath = f"{folderPath}/{picFilename}"
					fs.writefile(picFsPath, picture)
					picture.seek(0) # Allows re-reading picture
				except Exception:
					raise errors.InvalidAPIUsage("Picture wasn't correctly saved in filesystem", status_code=500)

				# Get default account ID
				if account is not None:
					accountId = account.id
				else:
					accountId = cursor.execute("SELECT id FROM accounts WHERE is_default").fetchone()[0]

				# Insert picture into database
				picId = runner_pictures.insertNewPictureInDatabase(
					conn,
					folderPath,
					picFilename,
					Image.open(picture),
					accountId
				)

				# Save position of picture in sequence
				try:
					cursor.execute(
						"INSERT INTO sequences_pictures(seq_id, rank, pic_id) VALUES(%s, %s, %s)",
						[collectionId, position, picId]
					)
				except UniqueViolation:
					raise errors.InvalidAPIUsage("Picture at given position already exist", status_code=409)

				conn.commit()

				# Start generating picture derivates (async)
				startPicturePostProcess.submit(collectionId, folderPath, picFilename, accountId, picId)

				# Return picture metadata
				return getCollectionItem(collectionId, picId)[0], 202, {
					"Content-Type": "application/json",
					"Location": url_for("stac.getCollectionItem", _external=True, collectionId=collectionId, itemId=picId)
				}


@bp.route('/collections/<uuid:collectionId>/geovisio_status')
def getCollectionImportStatus(collectionId):
	"""Retrieve import status of all pictures in sequence
	---
		parameters:
			- name: collectionId
			  in: path
			  type: string
			  description: ID of collection to retrieve
		responses:
			200:
				description: the pictures statuses
				schema:
					type: object
					properties:
						items:
							type: array
							items:
								type: object
								properties:
									id:
										type: string
									status:
										type: string
										enum:
											- ready
											- broken
											- preparing
									rank:
										type: integer
	"""

	with psycopg.connect(current_app.config['DB_URL'], row_factory=dict_row) as conn:
		with conn.cursor() as cursor:
			pictures = cursor.execute("""
				SELECT p.id, p.status, sp.rank
				FROM sequences_pictures sp
				JOIN pictures p on sp.pic_id = p.id
				WHERE sp.seq_id = %s AND p.status != 'hidden'
				ORDER BY sp.rank
			""", [collectionId]).fetchall()

			if len(pictures) == 0:
				raise errors.InvalidAPIUsage("Sequence is either empty or doesn't exists", status_code=404)

			return { "items": pictures }
