import pytest
import psycopg
import os
import re
from fs import open_fs
import configparser

from src import create_app, runner_pictures, db_migrations


@pytest.fixture
def dburl():
	# load test.env file if available
	import dotenv
	val = dotenv.load_dotenv("test.env")

	db = os.environ['DB_URL']

	db_migrations.update_db_schema(db, force=True)
	return db


@pytest.fixture
def app(dburl, tmp_path):
	app = create_app({
	  'TESTING': True,
	  'DB_URL': dburl,
	  'FS_URL': str(tmp_path),
	  'SERVER_NAME': 'localhost:5000',
	  'BLUR_STRATEGY': 'DISABLE',
	  'DERIVATES_STRATEGY': 'ON_DEMAND',
	  'WEBP_METHOD': 0
	})
	yield app


@pytest.fixture
def client(app):
	with app.app_context():
		with app.test_client() as client:
			yield client


@pytest.fixture
def runner(app):
	return app.test_cli_runner()


# Code for having at least one sequence in tests
FIXTURE_DIR = os.path.join(
	os.path.dirname(os.path.realpath(__file__)),
	'data'
)

SEQ_IMG = pytest.mark.datafiles(os.path.join(FIXTURE_DIR, '1.jpg'))
SEQ_IMG_FLAT = pytest.mark.datafiles(os.path.join(FIXTURE_DIR, 'c1.jpg'))

SEQ_IMGS = pytest.mark.datafiles(
	os.path.join(FIXTURE_DIR, '1.jpg'),
	os.path.join(FIXTURE_DIR, '2.jpg'),
	os.path.join(FIXTURE_DIR, '3.jpg'),
	os.path.join(FIXTURE_DIR, '4.jpg'),
	os.path.join(FIXTURE_DIR, '5.jpg')
)

SEQ_IMGS_FLAT = pytest.mark.datafiles(
	os.path.join(FIXTURE_DIR, 'b1.jpg'),
	os.path.join(FIXTURE_DIR, 'b2.jpg')
)

SEQ_IMGS_NOHEADING = pytest.mark.datafiles(
	os.path.join(FIXTURE_DIR, 'e1.jpg'),
	os.path.join(FIXTURE_DIR, 'e2.jpg'),
	os.path.join(FIXTURE_DIR, 'e3.jpg'),
	os.path.join(FIXTURE_DIR, 'e4.jpg'),
	os.path.join(FIXTURE_DIR, 'e5.jpg')
)

@pytest.fixture
def initSequence(initSequenceApp):
	def fct2(datafiles, preprocess = True):
		return initSequenceApp(datafiles, preprocess)[0]

	return fct2


@pytest.fixture
def initSequenceApp(tmp_path, dburl):
	seqPath = tmp_path / "seq1"
	seqPath.mkdir()

	def fct(datafiles, preprocess = True, blur = False):
		twoSeqs = os.path.isfile(datafiles / '1.jpg') and os.path.isfile(datafiles / 'b1.jpg')

		if twoSeqs:
			seq2Path = tmp_path / "seq2"
			seq2Path.mkdir()
			for f in os.listdir(datafiles):
				if f != "seq1" and f != "seq2":
					os.rename(datafiles / f, (seq2Path if f[0:1] == "b" else seqPath) / re.sub("^[a-z]+", "", f))
		else:
			for f in os.listdir(datafiles):
				if f != "seq1":
					os.rename(datafiles / f, seqPath / re.sub("^[a-z]+", "", f)) 
		with psycopg.connect(dburl) as db:
			with open_fs(str(tmp_path)) as fs:
				app = create_app({ 'TESTING': True, 'BLUR_STRATEGY': 'FAST' if blur else 'DISABLE', 'DERIVATES_STRATEGY': 'PREPROCESS' if preprocess else 'ON_DEMAND', 'DB_URL': dburl, 'FS_URL': 'osfs://'+str(datafiles), 'WEBP_METHOD': 0 })
				with app.app_context():
					runner_pictures.processSequence(fs, db, "seq1")
					if twoSeqs:
						runner_pictures.processSequence(fs, db, "seq2")

					with app.test_client() as client:
						return (client, app)

	return fct


@pytest.fixture(autouse=True)
def dbCleanup(dburl):
	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			cursor.execute("""
TRUNCATE TABLE sequences, sequences_pictures, pictures, cameras CASCADE;
""")


@pytest.fixture()
def defaultAccountID(dburl):
	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			accountID = cursor.execute("SELECT id from accounts where name = 'Default account'").fetchone()[0]
	return accountID


@pytest.fixture()
def bobAccountID(dburl):
	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			accountID = cursor.execute("SELECT id from accounts WHERE name = 'bob'").fetchone()
			if accountID:
				return accountID[0]
			accountID = cursor.execute("INSERT INTO accounts (name) VALUES ('bob') RETURNING id").fetchone()[0]
	return accountID
