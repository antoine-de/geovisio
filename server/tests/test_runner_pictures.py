import datetime
import os
import pytest
import psycopg
import re
import filecmp
import shutil
from uuid import UUID
from psycopg.rows import dict_row
from PIL import Image, ImageChops, ImageStat
from fs import open_fs
from . import conftest
from src import runner_pictures, pictures, create_app

FIXTURE_DIR = os.path.join(
	os.path.dirname(os.path.realpath(__file__)),
	'data'
)


def test_createSequenceFolder_simple(tmp_path):
	with open_fs(str(tmp_path)) as fs:
		res = runner_pictures.createSequenceFolder(fs)

		assert re.match(r'^geovisio_uploads_'+datetime.date.today().isoformat()+r'-\d{2}-\d{2}-\d{2}_0$', res)
		assert os.path.isdir(tmp_path / res)


def test_createSequenceFolder_overlapping(tmp_path):
	with open_fs(str(tmp_path)) as fs:
		# Create all folders for next seconds
		start = datetime.datetime.now()
		for i in range(0,10):
			n = start + datetime.timedelta(seconds = i)
			os.mkdir(str(tmp_path) + '/geovisio_uploads_'+n.strftime('%Y-%m-%d-%H-%M-%S')+'_0')

		# Create sequence folder
		res = runner_pictures.createSequenceFolder(fs)

		assert re.match(r'^geovisio_uploads_'+datetime.date.today().isoformat()+r'-\d{2}-\d{2}-\d{2}_1$', res)
		assert os.path.isdir(tmp_path / res)


def test_createSequenceFolder_overflow(tmp_path):
	with open_fs(str(tmp_path)) as fs:
		# Create many folders
		start = datetime.datetime.now()
		for i in range(0,10):
			n = start + datetime.timedelta(seconds = i)
			for s in range(0, 60):
				os.mkdir(str(tmp_path) + '/geovisio_uploads_'+n.strftime('%Y-%m-%d-%H-%M-%S')+'_'+str(s))

		with pytest.raises(runner_pictures.TooManySimilarSequencesException):
			runner_pictures.createSequenceFolder(fs)


def test_skipSequenceFolder():
	assert runner_pictures.skipSequenceFolder("gvs_mysequence")
	assert runner_pictures.skipSequenceFolder("geovisio_derivates")
	assert runner_pictures.skipSequenceFolder("ignore_mysequence")
	assert not runner_pictures.skipSequenceFolder("mysequence")


def test_listSequencesToProcess_empty(tmp_path, dburl):
	# Create a sequence subfolder with a single picture
	seq1 = tmp_path / "seq1"
	seq1pic1 = seq1 / "1.jpg"
	seq1pic1.parent.mkdir()
	seq1pic1.touch()
	seq1pic1.write_text("Some text")

	# Create geovisio_derivates folder
	gvs = tmp_path / "geovisio_derivates"
	gvs.mkdir()

	# Call function
	with open_fs(str(tmp_path)) as fs:
		with psycopg.connect(dburl) as db:
			res = runner_pictures.listSequencesToProcess(fs, db)
			assert res == ["seq1"]

def test_listSequencesToProcess_existing(tmp_path, dburl):
	# Create a sequence subfolder with a single picture
	seq1 = tmp_path / "seq1"
	seq1pic1 = seq1 / "1.jpg"
	seq1pic1.parent.mkdir()
	seq1pic1.touch()
	seq1pic1.write_text("Some text")

	# Create geovisio_derivates folder
	gvs = tmp_path / "geovisio_derivates"
	gvs.mkdir()

	# Make it already initialized in DB
	with psycopg.connect(dburl) as db:
		userId = db.execute("SELECT id FROM accounts WHERE is_default").fetchone()[0]
		db.execute("INSERT INTO sequences(folder_path, status, account_id) VALUES ('seq1', 'ready', %s)", [userId])

		# Call function
		with open_fs(str(tmp_path)) as fs:
			res = runner_pictures.listSequencesToProcess(fs, db)
			assert res == []


@conftest.SEQ_IMGS
@conftest.SEQ_IMGS_FLAT
@pytest.mark.parametrize(('singleSeq', 'full', 'db', 'cache'), (
	(True, False, False, False),
	(True, True, False, False),
	(True, False, True, False),
	(True, False, False, True),
	(False, False, False, False),
	(False, True, False, False),
	(False, False, True, False),
	(False, False, False, True),
))
def test_cleanup(datafiles, initSequenceApp, tmp_path, dburl, singleSeq, full, db, cache):
	client, app = initSequenceApp(datafiles)

	with app.app_context():
		with psycopg.connect(dburl) as db:
			sequences = []
			picsSeq1 = sorted([ str(p[0]) for p in db.execute("SELECT pic_id FROM sequences_pictures WHERE seq_id = (SELECT id FROM sequences WHERE folder_path='seq1')") ])
			picsSeq2 = sorted([ str(p[0]) for p in db.execute("SELECT pic_id FROM sequences_pictures WHERE seq_id = (SELECT id FROM sequences WHERE folder_path='seq2')") ])

			if singleSeq:
				sequences = [ "seq1" ]

			runner_pictures.cleanup(sequences, full, db, cache, False)

			if full or db:
				assert [ p[0] for p in db.execute("SELECT folder_path FROM sequences").fetchall() ] == (["seq2"] if singleSeq else [])
				if singleSeq:
					assert sorted([ str(p[0]) for p in db.execute("SELECT id FROM pictures").fetchall() ]) == picsSeq2
				else:
					assert len(db.execute("SELECT id FROM pictures").fetchall()) == 0
			else:
				assert [ p[0] for p in db.execute("SELECT folder_path FROM sequences").fetchall() ] == [ "seq1", "seq2" ]

			if full and not singleSeq:
				assert not os.path.isdir(datafiles / "geovisio_derivates")
			elif full or cache:
				if singleSeq:
					for p in picsSeq1:
						assert not os.path.isdir(datafiles / "geovisio_derivates" / p[0:2] / p)
					for p in picsSeq2:
						assert os.path.isdir(datafiles / "geovisio_derivates" / p[0:2] / p)
				else:
					assert len(os.listdir(datafiles / "geovisio_derivates")) == 0
			else:
				for p in picsSeq1:
					assert os.path.isdir(datafiles / "geovisio_derivates" / p[0:2] / p)
				for p in picsSeq2:
					assert os.path.isdir(datafiles / "geovisio_derivates" / p[0:2] / p)


@conftest.SEQ_IMGS
@conftest.SEQ_IMGS_FLAT
@pytest.mark.skipci
def test_cleanup_blur(datafiles, initSequenceApp, tmp_path, dburl):
	client, app = initSequenceApp(datafiles, blur = True)

	with app.app_context():
		with psycopg.connect(dburl) as db:
			sequences = [ "seq1" ]
			picsSeq1 = sorted([ str(p[0]) for p in db.execute("SELECT pic_id FROM sequences_pictures WHERE seq_id = (SELECT id FROM sequences WHERE folder_path='seq1')") ])
			picsSeq2 = sorted([ str(p[0]) for p in db.execute("SELECT pic_id FROM sequences_pictures WHERE seq_id = (SELECT id FROM sequences WHERE folder_path='seq2')") ])

			runner_pictures.cleanup(sequences, False, False, False, True)

			# Check DB and other derivates are untouched
			assert [ p[0] for p in db.execute("SELECT folder_path FROM sequences").fetchall() ] == [ "seq1", "seq2" ]
			for p in picsSeq1:
				assert os.path.isdir(datafiles / "geovisio_derivates" / p[0:2] / p)
				assert os.path.isfile(datafiles / "geovisio_derivates" / p[0:2] / p / "sd.webp")
				assert os.path.isfile(datafiles / "geovisio_derivates" / p[0:2] / p / "thumb.webp")
			for p in picsSeq2:
				assert os.path.isdir(datafiles / "geovisio_derivates" / p[0:2] / p)
				assert os.path.isfile(datafiles / "geovisio_derivates" / p[0:2] / p / "blur_mask.png")

			# Check blur masks are deleted
			for p in picsSeq1:
				assert not os.path.isfile(datafiles / "geovisio_derivates" / p[0:2] / p / "blur_mask.png")


@conftest.SEQ_IMGS
def test_processSequence(datafiles, initSequence, tmp_path, dburl, defaultAccountID):
	initSequence(datafiles)

	# Check results
	with psycopg.connect(dburl, row_factory=dict_row) as db2:
		# Sequence definition
		res0 = db2.execute("SELECT id, folder_path, status, metadata, account_id, ST_AsText(geom) AS geom FROM sequences").fetchall()[0]

		seqId = str(res0['id'])
		assert len(seqId) > 0
		# use regex because float precision may differ between systems
		expectedGeom = re.compile(r"^LINESTRING\(1\.919185441799\d+ 49\.00688961988\d+,1\.919189623000\d+ 49\.0068986458\d+,1\.919196360602\d+ 49\.00692625960\d+,1\.919199780601\d+ 49\.00695484980\d+,1\.919194019996\d+ 49\.00697341759\d+\)$")
		assert expectedGeom.match(res0['geom']) is not None
		assert res0['status'] == "ready"
		assert res0['folder_path'] == "seq1"
		assert res0['account_id'] == defaultAccountID

		# Pictures
		res1 = db2.execute("SELECT id, ts, status, metadata, account_id FROM pictures ORDER BY ts").fetchall()

		assert len(res1) == 5
		assert len(str(res1[0]['id'])) > 0
		assert res1[0]['ts'].timestamp() == 1627550214.0
		assert res1[0]['status'] == 'ready'
		assert res1[0]['metadata']['field_of_view'] == 360
		assert res1[0]['account_id'] == defaultAccountID

		picIds = []
		for rec in res1:
			picIds.append(str(rec['id']))

		# Sequences + pictures
		with db2.cursor() as cursor:
			res2 = cursor.execute("SELECT pic_id FROM sequences_pictures WHERE seq_id = %s ORDER BY rank", [seqId]).fetchall()
			resPicIds = [ str(f['pic_id']) for f in res2 ]

			assert resPicIds == picIds

		# Check destination folder structure
		for picId in picIds:
			basePath = tmp_path / "geovisio_derivates" / picId[0:2] / picId
			assert os.path.isdir(basePath)
			assert os.path.isdir(basePath / "tiles")
			assert os.path.isfile(basePath / "sd.webp")
			assert os.path.isfile(basePath / "thumb.webp")

		newSequencePicturesEntries = db2.execute("select rank from sequences_pictures inner join pictures on (pic_id = id) order by file_path asc").fetchall()
		assert newSequencePicturesEntries == [ {'rank': rank} for rank in range(1, len(newSequencePicturesEntries)+1) ]


@conftest.SEQ_IMGS_FLAT
def test_processSequence_flat(datafiles, initSequence, tmp_path, dburl, defaultAccountID):
	with psycopg.connect(dburl, row_factory=dict_row) as db2:
		# Add camera metadata
		db2.execute("INSERT INTO cameras(model, sensor_width) VALUES ('OLYMPUS IMAGING CORP. SP-720UZ', 6.16)")
		db2.commit()

		# Run processing
		app = initSequence(datafiles) # Calls runner_pictures.processSequence

		# Sequence definition
		res0 = db2.execute("SELECT id, folder_path, status, metadata, account_id, ST_AsText(geom) AS geom FROM sequences").fetchall()[0]

		seqId = str(res0['id'])
		assert len(seqId) > 0
		# use regex because float precision may differ between systems
		expectedGeom = re.compile(r"^LINESTRING\(-1\.949973106007\d+ 48\.139852239480\d+,-1\.949124581909\d+ 48\.13939279199\d+\)$")
		assert expectedGeom.match(res0['geom']) is not None
		assert res0['status'] == "ready"
		assert res0['folder_path'] == "seq1"
		assert res0['account_id'] == defaultAccountID

		# Pictures
		res1 = db2.execute("SELECT id, ts, status, metadata, account_id FROM pictures ORDER BY ts").fetchall()

		assert len(res1) == 2
		assert len(str(res1[0]['id'])) > 0
		assert res1[0]['ts'].timestamp() == 1429976177.0
		assert res1[0]['status'] == 'ready'
		assert res1[0]['metadata']['field_of_view'] == 67
		assert res1[0]['account_id'] == defaultAccountID

		picIds = []
		for rec in res1:
			picIds.append(str(rec['id']))

		# Check destination folder structure
		for picId in picIds:
			basePath = tmp_path / "geovisio_derivates" / picId[0:2] / picId
			assert os.path.isdir(basePath)
			assert not os.path.isdir(basePath / "tiles")
			assert os.path.isfile(basePath / "sd.webp")
			assert os.path.isfile(basePath / "thumb.webp")


@conftest.SEQ_IMGS_NOHEADING
def test_processSequence_noheading(datafiles, initSequence, tmp_path, dburl):
	with psycopg.connect(dburl) as db2:
		initSequence(datafiles, preprocess=False)

		# Sequence definition
		seqId = db2.execute("SELECT id FROM sequences").fetchone()[0]

		# Pictures
		res1 = db2.execute("SELECT file_path, heading, status, metadata FROM pictures ORDER BY ts").fetchall()

		expectedHeading = { "seq1/1.jpg": 277, "seq1/2.jpg": 272, "seq1/3.jpg": 272, "seq1/4.jpg": 270, "seq1/5.jpg": 270 }
		for r in res1:
			assert r[2] == "ready"
			assert expectedHeading[r[0]] == r[1]
			assert r[3].get('heading') is None


@conftest.SEQ_IMGS
def test_processSequence_customAssociatedAccount(datafiles, initSequence, tmp_path, dburl, bobAccountID):
	metadataFile = tmp_path / 'seq1' / 'metadata.txt'
	metadataFile.write_text("account-name = bob")
	initSequence(datafiles)

	# Check results
	with psycopg.connect(dburl, row_factory=dict_row) as db2:
		# Sequence definition
		res0 = db2.execute("SELECT id, folder_path, status, metadata, account_id, ST_AsText(geom) AS geom FROM sequences").fetchall()[0]

		seqId = str(res0['id'])
		assert len(seqId) > 0
		# use regex because float precision may differ between systems
		expectedGeom = re.compile(r"^LINESTRING\(1\.919185441799\d+ 49\.00688961988\d+,1\.919189623000\d+ 49\.0068986458\d+,1\.919196360602\d+ 49\.00692625960\d+,1\.919199780601\d+ 49\.00695484980\d+,1\.919194019996\d+ 49\.00697341759\d+\)$")
		assert expectedGeom.match(res0['geom']) is not None
		assert res0['status'] == "ready"
		assert res0['folder_path'] == "seq1"
		assert res0['account_id'] == bobAccountID

		# Pictures
		res1 = db2.execute("SELECT id, ts, status, metadata, account_id FROM pictures ORDER BY ts").fetchall()

		assert len(res1) == 5
		assert len(str(res1[0]['id'])) > 0
		assert res1[0]['ts'].timestamp() == 1627550214.0
		assert res1[0]['status'] == 'ready'
		assert res1[0]['metadata']['field_of_view'] == 360
		assert res1[0]['account_id'] == bobAccountID



@conftest.SEQ_IMGS
def test_processSequence_customAssociatedAccountID(datafiles, initSequence, tmp_path, dburl, bobAccountID):
	metadataFile = tmp_path / 'seq1' / 'metadata.txt'
	metadataFile.write_text(f"account-id = {bobAccountID}")
	initSequence(datafiles)

	with psycopg.connect(dburl, row_factory=dict_row) as db2:
		# All pictures should be associated to the account
		res1 = db2.execute("SELECT account_id FROM pictures ORDER BY ts").fetchall()

		assert len(res1) == 5
		for accountID in res1:
			assert accountID["account_id"] == bobAccountID


@pytest.fixture()
def addAdditionalBob(dburl):
	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			accountID = cursor.execute("INSERT INTO accounts (name) VALUES ('bob') RETURNING id").fetchone()[0]
			conn.commit()
			yield
			# remove the additional account
			cursor.execute("DELETE FROM accounts WHERE id = %s", [accountID])

@conftest.SEQ_IMGS
def test_processSequence_tooManyAccountWithSameName(datafiles, initSequence, tmp_path, dburl, bobAccountID, addAdditionalBob):
	# There is no unique constraint on account name since it can be provided by an external identity provider
	# if there are several account with the same name, use `account-id` instead of `account-name`
	metadataFile = tmp_path / 'seq1' / 'metadata.txt'
	metadataFile.write_text(f"account-name = bob")

	with pytest.raises(runner_pictures.TooManyAccountWithNameException) as e:
		initSequence(datafiles)
		assert e.account == 'bob'

@conftest.SEQ_IMGS
def test_processSequence_unknownCustomAssociatedAccount(datafiles, initSequence, tmp_path, dburl):
	metadataFile = tmp_path / 'seq1' / 'metadata.txt'
	metadataFile.write_text("account-name = unknown account")

	with pytest.raises(runner_pictures.UnknownAccountException) as e:
		initSequence(datafiles)
		assert e.account == 'unknown account'


@pytest.fixture()
def removeDefaultAccount(dburl):
	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			accountID = cursor.execute("UPDATE accounts SET is_default = false WHERE is_default = true RETURNING id").fetchone()[0]
			conn.commit()
			yield
			# put back the account at the end of the test
			cursor.execute("UPDATE accounts SET is_default = true WHERE id = %s", [accountID])


@conftest.SEQ_IMGS
def test_processSequence_noDefaultAccount(datafiles, initSequence, tmp_path, dburl, removeDefaultAccount):

	with pytest.raises(runner_pictures.NoAccountException) as e:
		initSequence(datafiles)


@conftest.SEQ_IMGS
def test_processSequence_invalidMetadataFile(datafiles, initSequence, tmp_path, dburl):
	"""Invalid metadata file raises a config parser error"""
	import configparser

	metadataFile = tmp_path / 'seq1' / 'metadata.txt'
	metadataFile.write_text("some file that is not valid")

	with pytest.raises(configparser.ParsingError):
		initSequence(datafiles)


@pytest.mark.datafiles(
	os.path.join(FIXTURE_DIR, '1.jpg'),
	os.path.join(FIXTURE_DIR, '2.jpg'),
	os.path.join(FIXTURE_DIR, '3.jpg'),
)
def test_reprocessSequence(datafiles, tmp_path, dburl):
	notModifiedFile = "1.jpg"
	deletedFile = "2.jpg"
	newFile = "3.jpg"

	# Prepare folder hierarchy
	sequenceFolder = "seq1"
	gvsPath = tmp_path / "gvs"
	gvsPath.mkdir()
	sequencePath = gvsPath / sequenceFolder
	sequencePath.mkdir()
	baseDerivPath = gvsPath / "geovisio_derivates"

	# Copy initial files
	shutil.copy(tmp_path / notModifiedFile, sequencePath / notModifiedFile)
	shutil.copy(tmp_path / deletedFile, sequencePath / deletedFile)

	app = create_app({ 'TESTING': True, 'BLUR_STRATEGY': 'DISABLE', 'DERIVATES_STRATEGY': 'PREPROCESS', 'DB_URL': dburl, 'FS_URL': 'osfs://'+str(datafiles) })

	with open_fs(str(gvsPath)) as fs:
		with psycopg.connect(dburl) as db:
			with app.app_context():
				# Do the initial sequence process
				runner_pictures.processSequence(fs, db, sequenceFolder)

				originalPictureEntries = db.execute("select id, file_path, status from pictures order by file_path asc").fetchall()
				originalSequenceId, originalSequenceStatus = db.execute("select id, status from sequences").fetchone()

				# Check database entries
				assert len(originalPictureEntries) == 2
				assert originalSequenceStatus == 'ready'
				# Check files
				assert originalPictureEntries[0][1] == sequenceFolder + '/' + notModifiedFile
				assert originalPictureEntries[1][1] == sequenceFolder + '/' + deletedFile
				for (id, file_path, pictureStatus) in originalPictureEntries:
					baseDerivPicturePath = baseDerivPath / str(id)[0:2] / str(id)
					assert pictureStatus == 'ready'
					assert os.path.exists(baseDerivPicturePath / "thumb.webp")
					assert os.path.exists(baseDerivPicturePath / "sd.webp")

				# Remove the 'deleted' file and create the 'new' file
				os.remove(sequencePath / deletedFile)
				shutil.copy(tmp_path / newFile, sequencePath / newFile)

				# Actually reprocess the sequence
				runner_pictures.reprocessSequence(fs, db, sequenceFolder, originalSequenceId)

				newPictureEntries = db.execute("select id, file_path, status from pictures order by file_path asc").fetchall()
				newSequenceEntries = db.execute("select id, status from sequences").fetchall()
				newSequencePicturesEntries = db.execute("select pic_id, seq_id from sequences_pictures").fetchall()

				# Check database entries
				assert len(newSequenceEntries) == 1
				assert newSequenceEntries[0] == (originalSequenceId, "ready")
				assert len(newPictureEntries) == 2
				assert newPictureEntries[0] == originalPictureEntries[0]         # check unmodified entry
				assert newPictureEntries[1][1] == sequenceFolder + '/' + newFile
				assert newPictureEntries[1][2] == 'ready'
				assert newPictureEntries[1][0] != originalPictureEntries[1][0]   # check that the new entry has a different id than the deleted one
				assert newSequencePicturesEntries == [ (entry[0], originalSequenceId) for entry in newPictureEntries ] # check that picture<->sequence links are fine
				# Check that ranks are sorted by picture name
				newSequencePicturesEntries = db.execute("select rank from sequences_pictures inner join pictures on (pic_id = id) order by file_path asc").fetchall()
				assert len(newSequencePicturesEntries) == 2
				assert newSequencePicturesEntries == [ (rank,) for rank in range(1, len(newSequencePicturesEntries)+1) ]

				# Check files
				for (id, file_path, pictureStatus) in newPictureEntries:
					baseDerivPicturePath = baseDerivPath / str(id)[0:2] / str(id)
					assert os.path.exists(baseDerivPicturePath / "thumb.webp")
					assert os.path.exists(baseDerivPicturePath / "sd.webp")
				deletedPicId = originalPictureEntries[1][0]
				deletedPicDericPicPath = baseDerivPath / str(deletedPicId)[0:2] / str(deletedPicId)
				assert not os.path.exists(deletedPicDericPicPath)
				# check that only 2 derivative folders remain (works if the 2 pictures have uuids with different first 2 digits)
				assert len(os.listdir(baseDerivPath)) == 2


@conftest.SEQ_IMG
def test_reprocessSequenceEmptied(datafiles, tmp_path, dburl):
	pictureName = "1.jpg"

	# Prepare folder hierarchy
	sequenceFolder = "seq1"
	gvsPath = tmp_path / "gvs"
	gvsPath.mkdir()
	sequencePath = gvsPath / sequenceFolder
	sequencePath.mkdir()
	baseDerivPath = gvsPath / "geovisio_derivates"

	os.rename(tmp_path / pictureName, sequencePath / pictureName)

	app = create_app({ 'TESTING': True, 'BLUR_STRATEGY': 'DISABLE', 'DERIVATES_STRATEGY': 'PREPROCESS', 'DB_URL': dburl, 'FS_URL': 'osfs://'+str(datafiles) })

	with open_fs(str(gvsPath)) as fs:
		with psycopg.connect(dburl) as db:
			with app.app_context():
				# Do the initial sequence process
				runner_pictures.processSequence(fs, db, sequenceFolder)
				originalSequenceEntry = db.execute("select id, status from sequences").fetchone()
				originalSequenceId, originalSequenceStatus = originalSequenceEntry

				assert originalSequenceStatus == 'ready'
				assert len(os.listdir(baseDerivPath)) == 1

				os.remove(sequencePath / pictureName)

				runner_pictures.reprocessSequence(fs, db, sequenceFolder, originalSequenceId)

				newSequenceEntry = db.execute("select status from sequences").fetchone()

				# An empty sequence should be in the 'broken' state
				assert newSequenceEntry[0] == 'broken'
				assert len(os.listdir(baseDerivPath)) == 0

@conftest.SEQ_IMGS
def test_reprocessSequenceChangeAssociatedAccount(datafiles, tmp_path, dburl, bobAccountID, defaultAccountID):
	pictureName = "1.jpg"

	# Prepare folder hierarchy
	sequenceFolder = "seq1"
	gvsPath = tmp_path / "gvs"
	gvsPath.mkdir()
	sequencePath = gvsPath / sequenceFolder
	sequencePath.mkdir()
	baseDerivPath = gvsPath / "geovisio_derivates"

	os.rename(tmp_path / pictureName, sequencePath / pictureName)

	metadataFilePath = sequencePath / 'metadata.txt'
	metadataFilePath.write_text("account-name = bob")

	app = create_app({ 'TESTING': True, 'BLUR_STRATEGY': 'DISABLE', 'DERIVATES_STRATEGY': 'PREPROCESS', 'DB_URL': dburl, 'FS_URL': 'osfs://'+str(datafiles) })

	with open_fs(str(gvsPath)) as fs:
		with psycopg.connect(dburl) as db:
			with app.app_context():
				# Do the initial sequence process
				runner_pictures.processSequence(fs, db, sequenceFolder)
				originalSequenceId, originalSequenceStatus, = db.execute("select id, status from sequences").fetchone()

				assert originalSequenceStatus == 'ready'
				assert len(os.listdir(baseDerivPath)) == 1

				associatedAccounts = db.execute("select distinct(account_id) from pictures").fetchall()

				assert associatedAccounts == [(bobAccountID,)]

				# we remove the metadata file, all pictures should be associated to the default account
				os.remove(metadataFilePath)

				runner_pictures.reprocessSequence(fs, db, sequenceFolder, originalSequenceId)

				newSequenceEntry, newSequenceUser = db.execute("select status, account_id from sequences").fetchone()
				assert newSequenceEntry == 'ready'
				assert newSequenceUser == defaultAccountID

				associatedAccounts = db.execute("select distinct(account_id) from pictures").fetchall()
				assert associatedAccounts == [(defaultAccountID,)]

@conftest.SEQ_IMGS
def test_reprocessSequenceChangeInvalidAssociatedAccount(datafiles, tmp_path, dburl, defaultAccountID):
	pictureName = "1.jpg"

	# Prepare folder hierarchy
	sequenceFolder = "seq1"
	gvsPath = tmp_path / "gvs"
	gvsPath.mkdir()
	sequencePath = gvsPath / sequenceFolder
	sequencePath.mkdir()
	baseDerivPath = gvsPath / "geovisio_derivates"

	os.rename(tmp_path / pictureName, sequencePath / pictureName)

	metadataFilePath = sequencePath / 'metadata.txt'
	metadataFilePath.write_text("account-name = unknown account")

	app = create_app({ 'TESTING': True, 'BLUR_STRATEGY': 'DISABLE', 'DERIVATES_STRATEGY': 'PREPROCESS', 'DB_URL': dburl, 'FS_URL': 'osfs://'+str(datafiles) })

	with open_fs(str(gvsPath)) as fs:
		with psycopg.connect(dburl) as db:
			with app.app_context():

				# Do the initial sequence process, it will fail because the metadata file is invalid
				with pytest.raises(runner_pictures.UnknownAccountException) as e:
					runner_pictures.processSequence(fs, db, sequenceFolder)
					assert e.account == 'unknown account'

				assert db.execute("select count(*) from sequences").fetchone()[0] == 0

				# import again, with a valid metadata file, it should work now
				metadataFilePath.write_text("account-name = Default account")

				# Note: since the first run has failed, we cannot do a 'reprocess', we should do a process
				runner_pictures.processSequence(fs, db, sequenceFolder)
				originalSequenceId, originalSequenceStatus, = db.execute("select id, status from sequences").fetchone()
				assert originalSequenceStatus == 'ready'

				# no custom metadata provided, all pictures should be associated to the default account
				associatedAccounts = db.execute("select distinct(account_id) from pictures").fetchall()
				assert associatedAccounts == [(defaultAccountID,)]

				# import another time, with again an invalid file, should be ko, and the initial sequence should be kept
				metadataFilePath.write_text("account-name = some other unkown account")
				# Do the initial sequence process, it will fail because the metadata file is invalid
				with pytest.raises(runner_pictures.UnknownAccountException) as e:
					runner_pictures.reprocessSequence(fs, db, sequenceFolder, originalSequenceId)
					assert e.account == 'some other unkown account'

				newSequenceEntry = db.execute("select status from sequences").fetchone()[0]
				assert newSequenceEntry == 'ready'


@conftest.SEQ_IMG
def test_processNewPicture(datafiles, tmp_path, dburl, defaultAccountID):
	# Prepare folder hierarchy
	gvsPath = tmp_path / "gvs"
	gvsPath.mkdir()
	seqPath = gvsPath / "seq1"
	seqPath.mkdir()
	os.rename(datafiles / "1.jpg", seqPath / "1.jpg")

	app = create_app({ 'TESTING': True, 'DB_URL': dburl, 'BLUR_STRATEGY': 'DISABLE', 'DERIVATES_STRATEGY': 'PREPROCESS', 'FS_URL': 'osfs://'+str(gvsPath) })

	# Run processing
	with app.app_context():
		with open_fs(str(gvsPath)) as fs:
			with psycopg.connect(dburl) as db:
				runner_pictures.processNewPicture(fs, db, "seq1", "1.jpg", defaultAccountID, app.config)
				db.commit()

				# Check results in database
				with psycopg.connect(dburl, row_factory=dict_row) as db2:
					res = db2.execute("SELECT id, ts, heading, ST_X(geom) AS lon, ST_Y(geom) AS lat, status, metadata FROM pictures").fetchone()
					assert len(str(res['id'])) > 0
					assert res['ts'].timestamp() == 1627550214.0
					assert res['heading'] == 349
					assert res['lon'] == 1.9191854417991367
					assert res['lat'] == 49.00688961988304
					assert res['status'] == 'ready'
					assert res['metadata']['width'] == 5760
					assert res['metadata']['height'] == 2880
					assert res['metadata']['cols'] == 8
					assert res['metadata']['rows'] == 4
					assert res['metadata'].get("lat") is None
					assert res['metadata'].get("lon") is None
					assert res['metadata'].get("ts") is None
					assert res['metadata'].get("heading") is None

					# Check files on disk
					baseDerivPath = gvsPath / "geovisio_derivates" / str(res['id'])[0:2] / str(res['id'])
					assert os.path.exists(baseDerivPath / "thumb.webp")
					assert os.path.exists(baseDerivPath / "sd.webp")
					assert os.path.exists(baseDerivPath / "tiles")


@pytest.mark.skipci
@conftest.SEQ_IMG
@pytest.mark.parametrize("blurstrategy", [ 'FAST', 'COMPROMISE', 'QUALITATIVE' ], ids=lambda name:name)
def test_processNewPicture_withBlur(datafiles, tmp_path, dburl, blurstrategy, defaultAccountID):
	# Prepare folder hierarchy
	gvsPath = tmp_path / "gvs"
	gvsPath.mkdir()
	seqPath = gvsPath / "seq1"
	seqPath.mkdir()
	os.rename(datafiles / "1.jpg", seqPath / "1.jpg")

	app = create_app({ 'TESTING': True, 'DB_URL': dburl, 'BLUR_STRATEGY': blurstrategy, 'DERIVATES_STRATEGY': 'PREPROCESS', 'FS_URL': 'osfs://'+str(gvsPath), 'MODELS_FS_URL': '../../models' })

	# Run processing
	with app.app_context() as context:
		with open_fs(str(gvsPath)) as fs:
			with psycopg.connect(dburl) as db:
				from src.blur import blur
				blur.blurPreinit(app.config)
				runner_pictures.processNewPicture(fs, db, "seq1", "1.jpg", defaultAccountID, app.config)
				db.commit()

				# Check results in database
				with psycopg.connect(dburl, row_factory=dict_row) as db2:
					res = db2.execute("SELECT id FROM pictures").fetchone()
					assert len(str(res['id'])) > 0

					# Check files on disk
					baseDerivPath = gvsPath / "geovisio_derivates" / str(res['id'])[0:2] / str(res['id'])
					picBlurPath = baseDerivPath / "blurred.webp"
					picSdPath = baseDerivPath / "sd.webp"
					assert os.path.exists(baseDerivPath / "thumb.webp")
					assert os.path.exists(picSdPath)
					assert os.path.exists(picBlurPath)
					assert os.path.exists(baseDerivPath / "tiles")

					# Check derivates are based on blurred version
					picBlur = Image.open(picBlurPath)
					picSdBlur = picBlur.resize((2048, int(picBlur.size[1]*2048/picBlur.size[0])))
					diff = ImageChops.difference(
						picSdBlur.convert("RGB"),
						Image.open(picSdPath).convert("RGB")
					)
					stat = ImageStat.Stat(diff)
					diff_ratio = sum(stat.mean) / (len(stat.mean) * 255) * 100
					assert diff_ratio <= 2


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, 'b1.jpg'))
def test_processNewPicture_flat(datafiles, tmp_path, dburl, defaultAccountID):
	# Prepare folder hierarchy
	gvsPath = tmp_path / "gvs"
	gvsPath.mkdir()
	seqPath = gvsPath / "seq1"
	seqPath.mkdir()
	os.rename(datafiles / "b1.jpg", seqPath / "b1.jpg")

	app = create_app({ 'TESTING': True, 'DB_URL': dburl, 'BLUR_STRATEGY': 'DISABLE', 'DERIVATES_STRATEGY': 'PREPROCESS', 'FS_URL': 'osfs://'+str(gvsPath) })

	# Run processing
	with app.app_context():
		with open_fs(str(gvsPath)) as fs:
			with psycopg.connect(dburl) as db:
				runner_pictures.processNewPicture(fs, db, "seq1", "b1.jpg", defaultAccountID, app.config)
				db.commit()

				# Check results in database
				with psycopg.connect(dburl, row_factory=dict_row) as db2:
					res = db2.execute("SELECT id, ts, heading, ST_X(geom) AS lon, ST_Y(geom) AS lat, status, metadata FROM pictures").fetchone()
					assert len(str(res['id'])) > 0
					assert res['ts'].timestamp() == 1429976268.0
					assert res['heading'] == 155
					assert res['lon'] == -1.9499731060073981
					assert res['lat'] == 48.139852239480945
					assert res['status'] == 'ready'
					assert res['metadata']['type'] == "flat"
					assert res['metadata']['width'] == 4288
					assert res['metadata']['height'] == 3216
					assert res['metadata'].get("lat") is None
					assert res['metadata'].get("lon") is None
					assert res['metadata'].get("ts") is None
					assert res['metadata'].get("heading") is None
					assert not 'cols' in res['metadata']

					# Check files on disk
					baseDerivPath = gvsPath / "geovisio_derivates" / str(res['id'])[0:2] / str(res['id'])
					assert os.path.exists(baseDerivPath / "thumb.webp")
					assert os.path.exists(baseDerivPath / "sd.webp")


@pytest.mark.datafiles(
	os.path.join(FIXTURE_DIR, '1.jpg'),
	os.path.join(FIXTURE_DIR, 'b1.jpg')
)
def test_reprocessPicture(datafiles, tmp_path, dburl, defaultAccountID):
	sequenceFolder = "seq1"
	originalPictureName = "1.jpg"
	newPictureName = "b1.jpg"

	# Prepare folder hierarchy
	processedPictureName = "picture.jpg"
	gvsPath = tmp_path / "gvs"
	gvsPath.mkdir()
	seqPath = gvsPath / sequenceFolder
	seqPath.mkdir()
	os.rename(datafiles / originalPictureName, seqPath / processedPictureName)

	app = create_app({ 'TESTING': True, 'DB_URL': dburl, 'BLUR_STRATEGY': 'DISABLE', 'DERIVATES_STRATEGY': 'PREPROCESS', 'FS_URL': 'osfs://'+str(gvsPath) })

	# Run processing
	with app.app_context():
		with open_fs(str(gvsPath)) as fs:
			with psycopg.connect(dburl) as db:
				# Process the picture once
				runner_pictures.processSequence(fs, db, sequenceFolder)
				db.commit()

				# Make sure that the picture exists on disk and in database
				(picId,) = db.execute("SELECT id FROM pictures").fetchone()
				baseDerivPath = gvsPath / "geovisio_derivates" / str(picId)[0:2] / str(picId)

				assert os.path.exists(baseDerivPath / "thumb.webp")
				assert os.path.exists(baseDerivPath / "sd.webp")

				(sequenceId,) = db.execute("select id from sequences").fetchone()

				# Delete generated files and replace the picture
				fs.removetree("geovisio_derivates/" + str(picId)[0:2])
				os.remove(seqPath / processedPictureName)
				os.rename(datafiles / newPictureName, seqPath / processedPictureName)

				# Actually reprocess the file
				runner_pictures.reprocessPicture(fs, db, sequenceFolder, processedPictureName, sequenceId, defaultAccountID, app.config)
				db.commit()

				# Make sure the picture entry did not change
				res = db.execute("SELECT id, status FROM pictures").fetchall()
				assert len(res) == 1
				assert res[0] == (picId, 'ready')
				# Make sure the picture files exist
				assert os.path.exists(baseDerivPath / "thumb.webp")
				assert os.path.exists(baseDerivPath / "sd.webp")


@conftest.SEQ_IMG
def test_insertNewPictureInDatabase(datafiles, tmp_path, dburl, defaultAccountID):
	picture = Image.open(str(datafiles / "1.jpg"))

	with psycopg.connect(dburl) as db:
		picId = runner_pictures.insertNewPictureInDatabase(db, "seq1", "1.jpg", picture, defaultAccountID)
		db.commit()

		with psycopg.connect(dburl, row_factory=dict_row) as db2:
			res = db2.execute("""
				SELECT id, ts, heading, ST_X(geom) AS lon, ST_Y(geom) AS lat, status, metadata
				FROM pictures
				WHERE id = %s
			""", [picId]).fetchone()
			assert len(str(res['id'])) > 0
			assert res['ts'].timestamp() == 1627550214.0
			assert res['heading'] == 349
			assert res['lon'] == 1.9191854417991367
			assert res['lat'] == 49.00688961988304
			assert res['status'] == 'preparing'
			assert res['metadata']['width'] == 5760
			assert res['metadata']['height'] == 2880
			assert res['metadata']['cols'] == 8
			assert res['metadata']['rows'] == 4
			assert res['metadata'].get("lat") is None
			assert res['metadata'].get("lon") is None
			assert res['metadata'].get("ts") is None
			assert res['metadata'].get("heading") is None

@conftest.SEQ_IMGS
def test_updateSequenceHeadings_unchanged(datafiles, initSequence, dburl):
	initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl, autocommit=True) as db:
		seqId = db.execute("SELECT id FROM sequences").fetchone()[0]
		picHeadings = {}
		for key, value in db.execute("SELECT id, heading FROM pictures").fetchall():
			picHeadings[key] = value

		runner_pictures.updateSequenceHeadings(db, seqId, 10, True)

		for id, heading,headingMetadata in db.execute("SELECT id, heading, metadata->>'heading' AS mh FROM pictures").fetchall():
			assert picHeadings[id] == heading
			assert headingMetadata is None


@conftest.SEQ_IMGS
def test_updateSequenceHeadings_updateAllExisting(datafiles, initSequence, dburl):
	initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl, autocommit=True) as db:
		seqId = db.execute("SELECT id FROM sequences").fetchone()[0]
		runner_pictures.updateSequenceHeadings(db, seqId, 10, False)

		expectedHeadings = { "seq1/1.jpg": 34, "seq1/2.jpg": 23, "seq1/3.jpg": 16, "seq1/4.jpg": 352, "seq1/5.jpg": 352 }
		for file, heading, headingMetadata in db.execute("select file_path, heading, metadata->>'heading' AS mh from pictures").fetchall():
			assert heading == expectedHeadings[file]
			assert headingMetadata is None


@conftest.SEQ_IMG
def test_processPictureFiles_noblur_preprocess(datafiles, tmp_path, dburl):
	derivatesDir = tmp_path / "geovisio_derivates"
	derivatesDir.mkdir()

	picDerDir = tmp_path / "gvs_picder"
	picDerDir.mkdir()

	seqPath = tmp_path / "seq1"
	seqPath.mkdir()
	os.rename(datafiles / "1.jpg", seqPath / "1.jpg")
	picId = UUID("9eaec817-5348-4852-9443-0d5e5a8d8b77")
	picture = Image.open(str(seqPath / "1.jpg"))

	appConfig = {"PIC_DERIVATES_DIR": "geovisio_derivates", "BLUR_STRATEGY": "DISABLE", "DERIVATES_STRATEGY": "PREPROCESS"}

	with open_fs(str(tmp_path)) as fs:
		runner_pictures.processPictureFiles(fs, picId, picture, appConfig)

		# Check folder has been created
		assert sorted(fs.listdir("/geovisio_derivates/9e/9eaec817-5348-4852-9443-0d5e5a8d8b77/")) == [ "sd.webp", "thumb.webp", "tiles" ]

		# Check content is same as generatePictureDerivates
		resPicDer = pictures.generatePictureDerivates(
			fs,
			picture,
			{'cols': 8, 'rows': 4, 'width': 5760, 'height': 2880},
			"/gvs_picder"
		)
		assert resPicDer is True
		assert sorted(fs.listdir("/geovisio_derivates/9e/9eaec817-5348-4852-9443-0d5e5a8d8b77/")) == sorted(fs.listdir("/gvs_picder/"))

		comparative = filecmp.cmpfiles(
			str(derivatesDir / "9e" / "9eaec817-5348-4852-9443-0d5e5a8d8b77"),
			str(picDerDir),
			filter(lambda i: i != "tiles", fs.listdir("/geovisio_derivates/9e/9eaec817-5348-4852-9443-0d5e5a8d8b77/"))
		)
		assert len(comparative[0]) > 0  # Match
		assert len(comparative[1]) == 0 # Mismatch
		assert len(comparative[2]) == 0 # Errors

		comparativeTiles = filecmp.cmpfiles(
			str(derivatesDir / "9e" / "9eaec817-5348-4852-9443-0d5e5a8d8b77" / "tiles"),
			str(picDerDir / "tiles"),
			fs.listdir("/geovisio_derivates/9e/9eaec817-5348-4852-9443-0d5e5a8d8b77/tiles/")
		)
		assert len(comparativeTiles[0]) > 0  # Match
		assert len(comparativeTiles[1]) == 0 # Mismatch
		assert len(comparativeTiles[2]) == 0 # Errors


@conftest.SEQ_IMG
def test_processPictureFiles_noblur_ondemand(datafiles, tmp_path, dburl):
	derivatesDir = tmp_path / "geovisio_derivates"
	derivatesDir.mkdir()

	picDerDir = tmp_path / "gvs_picder"
	picDerDir.mkdir()

	seqPath = tmp_path / "seq1"
	seqPath.mkdir()
	os.rename(datafiles / "1.jpg", seqPath / "1.jpg")
	picId = UUID("9eaec817-5348-4852-9443-0d5e5a8d8b77")
	picture = Image.open(str(seqPath / "1.jpg"))

	appConfig = {"PIC_DERIVATES_DIR": "geovisio_derivates", "BLUR_STRATEGY": "DISABLE", "DERIVATES_STRATEGY": "ON_DEMAND"}

	with open_fs(str(tmp_path)) as fs:
		runner_pictures.processPictureFiles(fs, picId, picture, appConfig)

		# No blurring + on-demand derivates = nothing done
		assert not fs.isdir("/geovisio_derivates/9e")


@pytest.mark.skipci
@conftest.SEQ_IMG
def test_processPictureFiles_blur_preprocess(datafiles, tmp_path, dburl):
	derivatesDir = tmp_path / "geovisio_derivates"
	derivatesDir.mkdir()

	seqPath = tmp_path / "seq1"
	seqPath.mkdir()
	os.rename(datafiles / "1.jpg", seqPath / "1.jpg")
	picId = UUID("9eaec817-5348-4852-9443-0d5e5a8d8b77")
	picture = Image.open(str(seqPath / "1.jpg"))

	appConfig = {"PIC_DERIVATES_DIR": "geovisio_derivates", "BLUR_STRATEGY": "FAST", "MODELS_FS_URL": '../models', "DERIVATES_STRATEGY": "PREPROCESS"}

	from src.blur.blur import blurPreinit
	blurPreinit(appConfig)

	with open_fs(str(tmp_path)) as fs:
		runner_pictures.processPictureFiles(fs, picId, picture, appConfig)

		# Check folder has been created
		assert sorted(fs.listdir("/geovisio_derivates/9e/9eaec817-5348-4852-9443-0d5e5a8d8b77/")) == [ "blur_mask.png", "blurred.webp", "sd.webp", "thumb.webp", "tiles" ]


@pytest.mark.skipci
@conftest.SEQ_IMG
def test_processPictureFiles_blur_ondemand(datafiles, tmp_path, dburl):
	derivatesDir = tmp_path / "geovisio_derivates"
	derivatesDir.mkdir()

	seqPath = tmp_path / "seq1"
	seqPath.mkdir()
	os.rename(datafiles / "1.jpg", seqPath / "1.jpg")
	picId = UUID("9eaec817-5348-4852-9443-0d5e5a8d8b77")
	picture = Image.open(str(seqPath / "1.jpg"))

	appConfig = {"PIC_DERIVATES_DIR": "geovisio_derivates", "BLUR_STRATEGY": "FAST", "MODELS_FS_URL": '../models', "DERIVATES_STRATEGY": "ON_DEMAND"}

	from src.blur.blur import blurPreinit
	blurPreinit(appConfig)

	with open_fs(str(tmp_path)) as fs:
		runner_pictures.processPictureFiles(fs, picId, picture, appConfig)

		# Blur + on-demand derivates = only generates blur mask
		# Check folder has been created
		assert sorted(fs.listdir("/geovisio_derivates/9e/9eaec817-5348-4852-9443-0d5e5a8d8b77/")) == [ "blur_mask.png" ]


@conftest.SEQ_IMG
def test_readPictureMetadata(datafiles):
	result = runner_pictures.readPictureMetadata(Image.open(str(datafiles) + "/1.jpg"))
	assert result == { "lat": 49.00688961988304, "lon": 1.9191854417991367, "ts": 1627550214.0, "heading": 349, "type": "equirectangular", "make": "GoPro", "model": "Max", "focal_length": 3 }


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, 'a1.jpg'))
def test_readPictureMetadata_negCoords(datafiles):
	result = runner_pictures.readPictureMetadata(Image.open(str(datafiles) + "/a1.jpg"))
	assert result == { "lat": 48.33756428166505, "lon": -1.9331088333333333, "ts": 1652453580.0, "heading": 32, "type": "equirectangular", "make": "GoPro", "model": "Max", "focal_length": 3 }


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, 'b1.jpg'))
def test_readPictureMetadata_flat(datafiles):
	result = runner_pictures.readPictureMetadata(Image.open(str(datafiles) + "/b1.jpg"))
	assert result == { "lat": 48.139852239480945, "lon": -1.9499731060073981, "ts": 1429976268.0, "heading": 155, "type": "flat", "make": "OLYMPUS IMAGING CORP.", "model": "SP-720UZ", "focal_length": 4.66 }


@conftest.SEQ_IMG_FLAT
def test_readPictureMetadata_flat2(datafiles):
	result = runner_pictures.readPictureMetadata(Image.open(str(datafiles) + "/c1.jpg"))
	assert result == { "lat": 48.85779642035038, "lon": 2.3392783047650747, "ts": 1430744932.0, "heading": 302, "type": "flat", "make": "Canon", "model": "EOS 6D0", "focal_length": 35.0 }


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, 'd1.jpg'))
def test_readPictureMetadata_xmpHeading(datafiles):
	result = runner_pictures.readPictureMetadata(Image.open(str(datafiles) + "/d1.jpg"))
	assert result == { "lat": 50.87070833333333, "lon": -1.5260916666666666, "ts": 1600008019.0, "heading": 67, "type": "equirectangular", "make": "Google", "model": "Pixel 3", "focal_length": None }

@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, 'e1.jpg'))
def test_readPictureMetadata_noHeading(datafiles):
	result = runner_pictures.readPictureMetadata(Image.open(str(datafiles) + "/e1.jpg"))
	assert result == { "lat": 48.15506638888889, "lon": -1.6844680555555556, "ts": 1666166194.0, "heading": None, "type": "flat", "make": "SONY", "model": "FDR-X1000V", "focal_length": 2.8 }

@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, 'img_Ricoh_Theta.jpg'))
def test_readPictureMetadata_ricoh_theta(datafiles):
	for f in datafiles.listdir():
		result = runner_pictures.readPictureMetadata(Image.open(str(f)))
		assert result == {
			'focal_length': 0.75,
			'heading': 270,
			'lat': 48.83930905577957,
			'lon': 2.3205357914890987,
			'make': 'RICOH',
			'model': 'THETA m15',
			'ts': 1458911533.0,
			'type': 'equirectangular'
   }

@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, 'img_V4MPack.jpg'))
def test_readPictureMetadata_v4mpack(datafiles):
	for f in datafiles.listdir():
		result = runner_pictures.readPictureMetadata(Image.open(str(f)))
		assert result == {
      		'focal_length': None,
			'heading': 64,
			'lat': 47.08506017299737,
			'lon': -1.2761512389983616,
			'make': 'STFMANI',
			'model': 'V4MPOD 1',
			'ts': 1555417213.0,
			'type': 'equirectangular'
   		}
