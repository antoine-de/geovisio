import pytest
from src import create_app
import os
import requests


@pytest.fixture
def auth_app(dburl, tmp_path, keycloak):
	"""Configure an app with keycloak auth"""
	app = create_app(
		{
			"TESTING": True,
			"DEBUG": True,
			"DB_URL": dburl,
			"FS_URL": str(tmp_path),
			"BLUR_STRATEGY": "DISABLE",
			"DERIVATES_STRATEGY": "ON_DEMAND",
			"WEBP_METHOD": 0,
			"OAUTH_PROVIDER": "oidc",
			"SECRET_KEY": "plop",
			"OIDC_URL": keycloak,
			"CLIENT_ID": "geovisio",
			"CLIENT_SECRET": "what_a_secret",
			"FORCE_AUTH_ON_UPLOAD": True,
		}
	)
	yield app


@pytest.fixture
def auth_client(auth_app):
	with auth_app.app_context():
		with auth_app.test_client() as client:
			yield client


@pytest.fixture
def server(auth_app):
	"""start a real server, listening to a port
	Used to be able to receive queries from keycloak
	"""
	import threading
	from werkzeug.serving import make_server

	port = 5005
	s = make_server("localhost", port, auth_app, threaded=True)
	t = threading.Thread(target=s.serve_forever)

	t.start()
	yield f"http://localhost:{port}"
	s.shutdown()


def wait_for_keycloak(url):
	import time

	waiting_time = 0.5
	for i in range(0, 40):
		print(f"waiting for keycloak to be available on {url} ...")
		try:
			r = requests.get(url)
			if r.status_code == 200:
				print(f"keycloak ready in {i*waiting_time}s")
				return
		except requests.ConnectionError:
			pass
		time.sleep(waiting_time)
	raise Exception("keycloak never starts")


@pytest.fixture(scope="module")
def keycloak():
	from testcontainers import compose

	root_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "../../..")
	override_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), "docker-compose-auth-test.yml")
	with compose.DockerCompose(
		root_dir,
		compose_file_name=[os.path.join(root_dir, "docker", "docker-compose-keycloak.yml"), override_file],
		pull=True,
	) as compose:
		host = compose.get_service_host("auth", 8080)
		port = compose.get_service_port("auth", 8080)
		keycloak_realm_url = f"http://{host}:{port}/realms/geovisio"
		wait_for_keycloak(keycloak_realm_url)

		yield keycloak_realm_url
		stdout, stderr = compose.get_logs()
		if stderr:
			print("Errors\n:{}".format(stderr))
