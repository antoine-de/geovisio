from urllib.parse import urlparse, quote
import requests
import pytest
from uuid import UUID
from src import auth
from ..conftest import SEQ_IMGS

# mark all tests in the module with the docker marker
pytestmark = [pytest.mark.docker, pytest.mark.skipci]


def test_auth_deactivated(client):
	# by default the auth is not activated, so the login routes should not exists
	assert client.get("/api/auth/login").status_code == 404
	assert client.get("/api/auth/redirect").status_code == 404
	assert client.get("/api/auth/logout").status_code == 404


def test_login(auth_client, auth_app):
	response = auth_client.get("/api/auth/login")
	assert response.status_code == 302

	location = response.headers["Location"]
	parsed_url = urlparse(location)

	assert parsed_url.path == "/realms/geovisio/protocol/openid-connect/auth"

	queries = {s.split("=")[0]: s.split("=")[1] for s in parsed_url.query.split("&")}

	assert queries["response_type"] == "code"
	assert queries["code_challenge_method"] == "S256"
	assert queries["client_id"] == "geovisio"
	assert "code_challenge" in queries
	assert "state" in queries
	assert "nonce" in queries
	assert queries["scope"] == "openid"
	assert queries["redirect_uri"] == quote("http://localhost/api/auth/redirect", safe="")


def _get_keycloak_authenticate_form_url(response):
	"""Little hack to parse keycloak HTML to get the url to the authenticate form"""
	import re

	url = re.search('action="(.*login-actions/authenticate[^"]*)"', response.text)
	assert url
	url = url.group(1).replace("&amp;", "&")
	return url


def test_login_with_redirect(server, keycloak, auth_app):
	with requests.session() as s:
		# we do a first query to login (following redirect ) (inside a requests session to keep the cookies)
		login = s.get(f"{server}/api/auth/login", allow_redirects=True)
		login.raise_for_status()
		assert login.status_code == 200
  
		assert _redirect_history(login) == [
			"/api/auth/login",
			"/realms/geovisio/protocol/openid-connect/auth",
		]

		# Then we authenticate on the keycloak to an already created user (defined in 'keycloak-realm.json')
		url = _get_keycloak_authenticate_form_url(login)
		r = s.post(
			url,
			data={"username": "elysee", "password": "my password"},
			headers={"Content-Type": "application/x-www-form-urlencoded"},
			allow_redirects=True,
		)
		r.raise_for_status()

		# we should be redirected to '/'
		assert r.url == "http://localhost:5005/"

		# user_id/user_name should be accessible via cookies
		# but the cookie should be set by the /redirect route
		assert _redirect_history(r) == [
			"/realms/geovisio/login-actions/authenticate",
			"/api/auth/redirect",
			"/"
		]
		set_cookie = r.history[-1].headers["Set-Cookie"]
		assert 'user_id=' in set_cookie
		assert 'user_name="Elysee r"' in set_cookie
  
		# Once logged in, we can query the protected api /api/users/me (using the session cookie)
		user_info = s.get(f"{server}/api/users/me", allow_redirects=True)
		user_info.raise_for_status()
		user_info_json = user_info.json()
		assert "id" in user_info_json
		assert user_info_json["name"] == "Elysee r"
		assert user_info_json["links"] == [{"href":"http://localhost:5005/api/users/me/catalog", "rel":"catalog", "type":"application/json"}]
  
		# we log out of the server
		r = s.get(f"{server}/api/auth/logout")
		r.raise_for_status()

		# then the next calls to /protected_api will relauch the oauth dance
		# Note: since for the moment we do not logout from the oauth provider
		# since it's not yet implemented in authlib (https://github.com/lepture/authlib/issues/292)
		# The oauth dance will succeed without having to post the keycloak form
		# If we handle a real logout, this test will have to be modified
		r = s.get(f"{server}/api/users/me", allow_redirects=True)

		assert _redirect_history(r) == [
			"/api/users/me",
			"/api/auth/login",
			"/realms/geovisio/protocol/openid-connect/auth",
			"/api/auth/redirect",
			"/api/users/me",
		]
		assert user_info_json["name"] == "Elysee r"
  
def _redirect_history(r):
    return [urlparse(h.url).path for h in (r.history + [r])]


def test_not_logged_sequence_creation(server, keycloak, auth_app):
	r = requests.post(f"{server}/api/collections", json={ "title": "Séquence" }, allow_redirects=True)
	assert r.status_code == 401

@SEQ_IMGS
def test_not_logged_picture_upload(server, keycloak, auth_app, datafiles):
    # Note: we can test this on an non existing collection since the authentication is checked before everything else
	r = requests.post(
		f"{server}/api/collections/00000000-0000-0000-0000-000000000000/items",
		data={"position": 1},
		files={"picture": (datafiles / "1.jpg").open("rb")}
	)
	assert r.status_code == 401


@SEQ_IMGS
def test_logged_upload(server, keycloak, auth_app, datafiles):
	with requests.session() as s:
		# we do a first query to login (following redirect ) (inside a requests session to keep the cookies)
		login = s.get(f"{server}/api/auth/login", allow_redirects=True)
		login.raise_for_status()
		assert login.status_code == 200
  
		assert _redirect_history(login) == [
			"/api/auth/login",
			"/realms/geovisio/protocol/openid-connect/auth",
		]
		# This should ask us for login, and the login has been set as mandatory for upload in the app config
		# Then we authenticate on the keycloak to an already created user (defined in 'keycloak-realm.json')
		url = _get_keycloak_authenticate_form_url(login)
		authentication_response = s.post(
			url,
			data={"username": "elysee", "password": "my password"},
			headers={"Content-Type": "application/x-www-form-urlencoded"},
			allow_redirects=True,
		)
		authentication_response.raise_for_status()
		assert _redirect_history(authentication_response) == [
			"/realms/geovisio/login-actions/authenticate",
			"/api/auth/redirect",
			"/",
		]

		sequence_creation_response = s.post(f"{server}/api/collections", json={ "title": "Séquence" }, allow_redirects=True)
		sequence_creation_response.raise_for_status()

		# since we have already been authentified, the cookie should be here, and we should not be asked to authenticate again
		assert sequence_creation_response.history == []
  
		# the sequence should be associated to the user
		assert sequence_creation_response.json()['providers'] == [
			{
				"name": "Elysee r",
				"roles": ["producer"]
			}
		]

		sequence = sequence_creation_response.headers["Location"]
  
		# we then upload a pic to the sequence
		upload_response = s.post(
			f"{sequence}/items",
			data={"position": 1},
			files={"picture": (datafiles / "1.jpg").open("rb")}
		)

		print(f"response = {upload_response.json()}")
		# same, here, no oauth dance needed
		assert upload_response.history == []
		assert upload_response.status_code == 202
  
		# and we should be able to get those pictures
		pictures_location = upload_response.headers['Location']

		# we do not use the session, we do an unauthenticated call, this should be enough since the pictures are public
		pic_response = requests.get(pictures_location)
		j = pic_response.json()

		# the picture should be associated to the user
		assert j['providers'] == [
			{
				"name": "Elysee r",
				"roles": ["producer"]
			}
		]
