from flask import json
import pytest
import psycopg
import re
from pystac import Catalog, Collection, ItemCollection, Item
from src import create_app, stac
from datetime import datetime
from uuid import UUID
from . import conftest
from urllib.parse import urlencode
import time
import os

STAC_VERSION = "1.0.0-rc.2"

def test_landing(client):
	response = client.get('/api/')
	data = json.loads(response.get_data(as_text=True))

	assert response.status_code == 200
	assert data['type'] == 'Catalog'
	ctl = Catalog.from_dict(data)
	assert len(ctl.links) > 0
	assert ctl.title == "GeoVisio STAC API"
	assert ctl.id == "geovisio"
	assert ctl.extra_fields.get("extent") is None
	assert ctl.get_links("self")[0].get_absolute_href() == "http://localhost:5000/api/"


@conftest.SEQ_IMGS
def test_landing_extent(datafiles, initSequence):
	client = initSequence(datafiles, preprocess=False)
	response = client.get('/api/')
	data = json.loads(response.get_data(as_text=True))

	assert response.status_code == 200
	assert data['type'] == 'Catalog'
	ctl = Catalog.from_dict(data)
	assert len(ctl.links) > 0

	assert len(ctl.extra_fields["extent"]["temporal"]["interval"]) == 1
	assert len(ctl.extra_fields["extent"]["temporal"]["interval"][0]) == 2
	assert re.match(r'^2021-07-29T', ctl.extra_fields["extent"]["temporal"]["interval"][0][0])
	assert re.match(r'^2021-07-29T', ctl.extra_fields["extent"]["temporal"]["interval"][0][1])
	assert ctl.extra_fields["extent"]["spatial"] == { "bbox": [[1.9191854000091553, 49.00688934326172, 1.919199824333191, 49.00697708129883]] }



def test_conformance(client):
	response = client.get('/api/conformance')
	data = json.loads(response.get_data(as_text=True))

	assert response.status_code == 200
	assert data['conformsTo'] == stac.CONFORMANCE_LIST


def test_dbSequenceToStacCollection(client):
	dbSeq = {
		"id": UUID('{12345678-1234-5678-1234-567812345678}'),
		"name": "Test sequence",
		"minx": -1.0,
		"maxx": 1.0,
		"miny": -2.0,
		"maxy": 2.0,
		"mints": datetime.fromisoformat("2020-01-01T12:50:37+00:00"),
		"maxts": datetime.fromisoformat("2020-01-01T13:30:42+00:00"),
		"account_name": "Default account"
	}

	res = stac.dbSequenceToStacCollection(dbSeq)

	assert res['type'] == "Collection"
	assert res['stac_version'] == STAC_VERSION
	assert res['id'] == '12345678-1234-5678-1234-567812345678'
	assert res['title'] == "Test sequence"
	assert res['description'] == "A sequence of geolocated pictures"
	assert res['providers'] == [
		{"name": "Default account", "roles": ["producer"]},
	]
	assert res['keywords'] == ["pictures", "Test sequence"]
	assert res['license'] == "proprietary"
	assert res['extent']['spatial']['bbox'] == [[-1.0,-2.0,1.0,2.0]]
	assert res['extent']['temporal']['interval'] == [["2020-01-01T12:50:37+00:00", "2020-01-01T13:30:42+00:00"]]
	assert len(res['links']) == 4


def test_dbSequenceToStacCollectionEmtpyTemporalInterval(client):
	dbSeq = {
		"id": UUID('{12345678-1234-5678-1234-567812345678}'),
		"name": "Test sequence",
		"minx": -1.0,
		"maxx": 1.0,
		"miny": -2.0,
		"maxy": 2.0,
		"mints": None,
		"account_name": "Default account"
	}

	res = stac.dbSequenceToStacCollection(dbSeq)

	assert res['type'] == "Collection"
	assert res['stac_version'] == STAC_VERSION
	assert res['id'] == '12345678-1234-5678-1234-567812345678'
	assert res['title'] == "Test sequence"
	assert res['description'] == "A sequence of geolocated pictures"
	assert res['providers'] == [
		{"name": "Default account", "roles": ["producer"]},
	]
	assert res['keywords'] == ["pictures", "Test sequence"]
	assert res['license'] == "proprietary"
	assert res['extent']['spatial']['bbox'] == [[-1.0,-2.0,1.0,2.0]]
	assert res['extent']['temporal']['interval'] == [[None, None]]
	assert len(res['links']) == 4


def test_dbSequenceToStacCollectionEmptyBbox(client):
	dbSeq = {
		"id": UUID('{12345678-1234-5678-1234-567812345678}'),
		"name": "Test sequence",
		"minx": None,
		"maxx": None,
		"miny": None,
		"maxy": None,
		"mints": datetime.fromisoformat("2020-01-01T12:50:37+00:00"),
		"maxts": datetime.fromisoformat("2020-01-01T13:30:42+00:00"),
		"account_name": "Default account"
	}

	res = stac.dbSequenceToStacCollection(dbSeq)

	assert res['type'] == "Collection"
	assert res['stac_version'] == STAC_VERSION
	assert res['id'] == '12345678-1234-5678-1234-567812345678'
	assert res['title'] == "Test sequence"
	assert res['description'] == "A sequence of geolocated pictures"
	assert res['providers'] == [
		{"name": "Default account", "roles": ["producer"]},
	]
	assert res['keywords'] == ["pictures", "Test sequence"]
	assert res['license'] == "proprietary"
	assert res['extent']['spatial']['bbox'] == [[-180.0, -90.0, 180.0, 90.0]]


def test_collectionsEmpty(client):
	response = client.get('/api/collections')
	data = json.loads(response.get_data(as_text=True))

	assert response.status_code == 200
	assert len(data['collections']) == 0
	assert len(data['links']) == 3


@conftest.SEQ_IMGS
def test_user_catalog(datafiles, dburl, initSequence):
	client = initSequence(datafiles, preprocess=False)

	# Get user ID
	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			userId, userName = cursor.execute("SELECT id, name FROM accounts WHERE is_default").fetchone()

			response = client.get('/api/users/' + str(userId) + '/catalog')
			data = json.loads(response.get_data(as_text=True))

			assert response.status_code == 200
			assert data['type'] == 'Catalog'
			ctl = Catalog.from_dict(data)
			assert len(ctl.links) > 0
			assert ctl.title == userName+"'s sequences"
			assert ctl.id == f"user:{userId}"
			assert ctl.description == "List of all sequences of user "+userName
			assert ctl.extra_fields.get("extent") is None
			assert ctl.get_links("self")[0].get_absolute_href() == "http://localhost/api/users/" + str(userId) + "/catalog/"



@conftest.SEQ_IMGS
def test_collections(datafiles, initSequence):
	client = initSequence(datafiles, preprocess=False)

	response = client.get('/api/collections')
	data = json.loads(response.get_data(as_text=True))

	assert response.status_code == 200

	assert len(data['collections']) == 1
	assert len(data['links']) == 3

	print(data['collections'][0])
	clc = Collection.from_dict(data['collections'][0])

	assert data['collections'][0]['type'] == "Collection"
	assert data['collections'][0]['stac_version'] == STAC_VERSION
	assert len(data['collections'][0]['id']) > 0
	assert len(data['collections'][0]['title']) > 0
	assert data['collections'][0]['description'] == "A sequence of geolocated pictures"
	assert len(data['collections'][0]['keywords']) > 0
	assert len(data['collections'][0]['license']) > 0
	assert len(data['collections'][0]['extent']['spatial']['bbox'][0]) == 4
	assert len(data['collections'][0]['extent']['temporal']['interval'][0]) == 2
	assert len(data['collections'][0]['links']) == 4


def test_collectionMissing(client):
	response = client.get('/api/collections/00000000-0000-0000-0000-000000000000')
	assert response.status_code == 404


@conftest.SEQ_IMGS
def test_collectionById(datafiles, initSequence, dburl):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId = cursor.execute("SELECT id FROM sequences LIMIT 1").fetchone()[0]

			response = client.get('/api/collections/'+str(seqId))
			data = json.loads(response.get_data(as_text=True))

			assert response.status_code == 200
			clc = Collection.from_dict(data)


@conftest.SEQ_IMGS
def test_items(datafiles, initSequence, dburl):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId = cursor.execute("SELECT id FROM sequences LIMIT 1").fetchone()[0]

			response = client.get('/api/collections/' + str(seqId) + '/items')
			data = json.loads(response.get_data(as_text=True))

			assert response.status_code == 200

			assert data['type'] == "FeatureCollection"
			assert len(data['features']) == 5
			assert len(data['links']) == 3

			clc = ItemCollection.from_dict(data)
			assert len(clc) == 5

			# Check if items have next/prev picture info
			i = 0
			for item in clc:
				nbPrev = len([ l for l in item.links if l.rel == "prev" ])
				nbNext = len([ l for l in item.links if l.rel == "next" ])
				if i == 0:
					assert nbPrev == 0
					assert nbNext == 1
				elif i == len(clc) - 1:
					assert nbPrev == 1
					assert nbNext == 0
				else:
					assert nbPrev == 1
					assert nbNext == 1

				i += 1

			# Make one picture not available
			picHidden = data['features'][0]['id']
			cursor.execute("UPDATE pictures SET status = 'hidden' WHERE id = %s", [picHidden])
			conn.commit()

			response = client.get('/api/collections/' + str(seqId) + '/items')
			data = json.loads(response.get_data(as_text=True))

			assert response.status_code == 200

			assert data['type'] == "FeatureCollection"
			assert len(data['features']) == 4
			picIds = [ f['id'] for f in data['features'] ]
			assert picHidden not in picIds
			assert data['features'][0]['providers'] == [
				{"name": "Default account", "roles": ["producer"]},
			]


@conftest.SEQ_IMGS
def test_item(datafiles, initSequence, dburl):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId, picId = cursor.execute("SELECT seq_id, pic_id FROM sequences_pictures WHERE rank = 1").fetchone()

			response = client.get('/api/collections/' + str(seqId) + '/items/' + str(picId))
			data = json.loads(response.get_data(as_text=True))

			assert response.status_code == 200

			assert data['type'] == "Feature"
			assert data['geometry']['type'] == 'Point'
			assert len(str(data['id'])) > 0
			assert re.match(r'^2021-07-29T', data['properties']['datetime'])
			assert data['properties']['view:azimuth'] >= 0
			assert data['properties']['view:azimuth'] <= 360
			assert re.match(r'^https?://.*/api/pictures/'+str(picId)+r'/tiled/\{TileCol\}_\{TileRow\}.jpg$', data['asset_templates']['tiles']['href'])
			assert re.match(r'^https?://.*/api/pictures/'+str(picId)+r'/tiled/\{TileCol\}_\{TileRow\}.webp$', data['asset_templates']['tiles_webp']['href'])
			assert re.match(r'^https?://.*/api/pictures/'+str(picId)+'/hd.jpg$', data['assets']['hd']['href'])
			assert re.match(r'^https?://.*/api/pictures/'+str(picId)+'/sd.jpg$', data['assets']['sd']['href'])
			assert re.match(r'^https?://.*/api/pictures/'+str(picId)+'/hd.webp$', data['assets']['hd_webp']['href'])
			assert re.match(r'^https?://.*/api/pictures/'+str(picId)+'/sd.webp$', data['assets']['sd_webp']['href'])
			assert re.match(r'^https?://.*/api/pictures/'+str(picId)+'/thumb.jpg$', data['assets']['thumb']['href'])
			assert re.match(r'^https?://.*/api/pictures/'+str(picId)+'/thumb.webp$', data['assets']['thumb_webp']['href'])
			assert data['properties']['tiles:tile_matrix_sets']['geovisio']['tileMatrix'][0]['tileWidth'] == 720
			assert data['properties']['tiles:tile_matrix_sets']['geovisio']['tileMatrix'][0]['tileHeight'] == 720
			assert data['properties']['tiles:tile_matrix_sets']['geovisio']['tileMatrix'][0]['matrixHeight'] == 4
			assert data['properties']['tiles:tile_matrix_sets']['geovisio']['tileMatrix'][0]['matrixWidth'] == 8
			assert data['properties']['pers:interior_orientation']['camera_manufacturer'] == "GoPro"
			assert data['properties']['pers:interior_orientation']['camera_model'] == "Max"
			assert data['properties']['pers:interior_orientation']['field_of_view'] == 360
			assert data['providers'] == [
				{"name": "Default account", "roles": ["producer"]},
			]

			item = Item.from_dict(data)
			assert len(item.links) == 5
			assert len([ l for l in item.links if l.rel == "next"]) == 1

			# Make picture not available
			cursor.execute("UPDATE pictures SET status = 'hidden' WHERE id = %s", [picId])
			conn.commit()

			response = client.get('/api/collections/' + str(seqId) + '/items/' + str(picId))
			assert response.status_code == 404


@conftest.SEQ_IMGS_FLAT
def test_item_flat(datafiles, initSequence, dburl):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId, picId = cursor.execute("SELECT seq_id, pic_id FROM sequences_pictures WHERE rank = 1").fetchone()

			response = client.get('/api/collections/' + str(seqId) + '/items/' + str(picId))
			data = json.loads(response.get_data(as_text=True))

			assert response.status_code == 200

			assert data['type'] == "Feature"
			assert data['geometry']['type'] == 'Point'
			assert len(str(data['id'])) > 0
			assert re.match(r'^2015-04-25T', data['properties']['datetime'])
			assert data['properties']['view:azimuth'] >= 0
			assert data['properties']['view:azimuth'] <= 360
			assert re.match(r'^https?://.*/api/pictures/'+str(picId)+'/hd.jpg$', data['assets']['hd']['href'])
			assert re.match(r'^https?://.*/api/pictures/'+str(picId)+'/sd.jpg$', data['assets']['sd']['href'])
			assert re.match(r'^https?://.*/api/pictures/'+str(picId)+'/hd.webp$', data['assets']['hd_webp']['href'])
			assert re.match(r'^https?://.*/api/pictures/'+str(picId)+'/sd.webp$', data['assets']['sd_webp']['href'])
			assert re.match(r'^https?://.*/api/pictures/'+str(picId)+'/thumb.jpg$', data['assets']['thumb']['href'])
			assert re.match(r'^https?://.*/api/pictures/'+str(picId)+'/thumb.webp$', data['assets']['thumb_webp']['href'])
			assert 'assert_templates' not in data
			assert 'tiles:tile_matrix_sets' not in data['properties']
			assert data['properties']['pers:interior_orientation']['camera_manufacturer'] == "OLYMPUS IMAGING CORP."
			assert data['properties']['pers:interior_orientation']['camera_model'] == "SP-720UZ"
			assert 'field_of_view' not in data['properties']['pers:interior_orientation'] # Not in cameras DB

			item = Item.from_dict(data)
			assert len(item.links) == 5
			assert len([ l for l in item.links if l.rel == "next"]) == 1


@conftest.SEQ_IMG_FLAT
def test_item_flat_fov(datafiles, initSequence, dburl):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId, picId = cursor.execute("SELECT seq_id, pic_id FROM sequences_pictures WHERE rank = 1").fetchone()

			response = client.get('/api/collections/' + str(seqId) + '/items/' + str(picId))
			data = json.loads(response.get_data(as_text=True))

			assert response.status_code == 200

			assert len(str(data['id'])) > 0
			assert data['properties']['pers:interior_orientation']['camera_manufacturer'] == "Canon"
			assert data['properties']['pers:interior_orientation']['camera_model'] == "EOS 6D0"
			assert 'field_of_view' not in data['properties']['pers:interior_orientation'] # Not in cameras DB


@conftest.SEQ_IMG_FLAT
def test_item_missing_all_metadata(datafiles, initSequence, dburl):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId, picId = cursor.execute("SELECT seq_id, pic_id FROM sequences_pictures WHERE rank = 1").fetchone()

			# Remove EXIF metadata from DB
			cursor.execute("UPDATE pictures SET metadata = %s WHERE id = %s", [
				'{"ts": 1430744932.0, "lat": 48.85779642035038, "lon": 2.3392783047650747, "type": "flat", "width": 4104, "height": 2736, "heading": 302}',
				picId
			])
			conn.commit()

			response = client.get('/api/collections/' + str(seqId) + '/items/' + str(picId))
			data = json.loads(response.get_data(as_text=True))

			assert response.status_code == 200

			assert len(str(data['id'])) > 0
			assert len(data['properties']['pers:interior_orientation']) == 0


@conftest.SEQ_IMG_FLAT
@pytest.mark.parametrize(
	('status', 'httpCode'), (
	( 'ready',        200),
	('hidden',        404),
	('preparing',     102),
	('broken',        500)
))
def test_item_status_httpcode(datafiles, initSequence, dburl, status, httpCode):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId, picId = cursor.execute("SELECT seq_id, pic_id FROM sequences_pictures WHERE rank = 1").fetchone()

			# Remove EXIF metadata from DB
			cursor.execute("UPDATE pictures SET status = %s WHERE id = %s", [
				status,
				picId
			])
			conn.commit()

			response = client.get('/api/collections/' + str(seqId) + '/items/' + str(picId))
			assert response.status_code == httpCode


@conftest.SEQ_IMG_FLAT
def test_item_missing_partial_metadata(datafiles, initSequence, dburl):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId, picId = cursor.execute("SELECT seq_id, pic_id FROM sequences_pictures WHERE rank = 1").fetchone()

			# Remove EXIF metadata from DB
			cursor.execute("UPDATE pictures SET metadata = %s WHERE id = %s", [
				'{"ts": 1430744932.0, "lat": 48.85779642035038, "lon": 2.3392783047650747, "make": "Canon", "type": "flat", "width": 4104, "height": 2736, "heading": 302}',
				picId
			])
			conn.commit()

			response = client.get('/api/collections/' + str(seqId) + '/items/' + str(picId))
			data = json.loads(response.get_data(as_text=True))

			assert response.status_code == 200

			assert len(str(data['id'])) > 0
			assert data['properties']['pers:interior_orientation'] == {
				"camera_manufacturer": "Canon"
			}


intersectsGeojson1 = json.dumps({ "type": "Polygon", "coordinates": [[
	[ 1.9191969931125639, 49.00691313179996 ],
	[ 1.9191332906484602, 49.00689685694783 ],
	[ 1.9191691651940344, 49.00687024535389 ],
	[ 1.919211409986019, 49.006892018477274 ],
	[ 1.9191969931125639, 49.00691313179996 ]
]]})
intersectsGeojson2 = json.dumps({ "type": "Point", "coordinates": [ 1.919185442, 49.00688962 ] })

@pytest.mark.parametrize(
	('limit', 'bbox', 'datetime', 'intersects', 'ids', 'collections', 'httpCode', 'validRanks'), (
	(   None,   None,       None,         None,  None,          None,        200,  [1,2,3,4,5]),
	(      2,   None,       None,         None,  None,          None,        200,         None),
	(     -1,   None,       None,         None,  None,          None,        400,         None),
	(  99999,   None,       None,         None,  None,          None,        400,         None),
	(  "bla",   None,       None,         None,  None,          None,        400,         None),
	(   None, [0,0,1,1],    None,         None,  None,          None,        200,           []),
	(   None, "[0,0,1,1",   None,         None,  None,          None,        400,         None),
	(   None,    [1],       None,         None,  None,          None,        400,         None),
	(None, [1.919185,49.00688,1.919187,49.00690], None, None,  None, None,   200,          [1]),
	(   None,   None, "2021-07-29T11:16:54+02", None, None,     None,        200,          [1]),
	(   None,   None, "2021-07-29T00:00:00Z/..", None, None,    None,        200,  [1,2,3,4,5]),
	(   None,   None, "../2021-07-29T00:00:00Z", None, None,    None,        200,           []),
	(   None,   None, "2021-01-01T00:00:00Z/2021-07-29T11:16:58+02", None, None, None, 200, [1,2,3]),
	(   None,   None, "2021-01-01T00:00:00Z/", None, None,      None,        400,         None),
	(   None,   None, "/2021-01-01T00:00:00Z", None, None,      None,        400,         None),
	(   None,   None,       "..",         None,  None,          None,        400,         None),
	(   None,   None, "2021-07-29TNOTATIME", None, None,        None,        400,         None),
	(   None,   None,       None, intersectsGeojson1,  None,    None,        200,        [1,2]),
	(   None,   None,       None, intersectsGeojson2,  None,    None,        200,          [1]),
	(   None,   None,       None, "{ 'broken': ''",  None,      None,        400,         None),
	(   None,   None,       None, "{ 'type': 'Feature' }",  None, None,      400,         None),
	(   None,   None,       None,         None, [1,2],          None,        200,        [1,2]),
	(   None,   None,       None,         None,  None,          True,        200,  [1,2,3,4,5]),
))
@conftest.SEQ_IMGS
def test_search(datafiles, initSequence, dburl, limit, bbox, datetime, intersects, ids, collections, httpCode, validRanks):
	client = initSequence(datafiles, preprocess=False)

	# Transform input ranks into picture ID to pass to query
	if ids is not None and len(ids) > 0:
		with psycopg.connect(dburl) as conn:
			with conn.cursor() as cursor:
				ids = json.dumps(cursor.execute("SELECT array_to_json(array_agg(pic_id::varchar)) FROM sequences_pictures WHERE rank = ANY(%s)", [ids]).fetchone()[0])

	# Retrieve sequence ID to pass into collections in query
	if collections is True:
		with psycopg.connect(dburl) as conn:
			with conn.cursor() as cursor:
				collections = json.dumps([ cursor.execute("SELECT id::varchar FROM sequences").fetchone()[0] ])

	query = {"limit": limit, "bbox": bbox, "datetime": datetime, "intersects": intersects, "ids": ids, "collections": collections}
	query = dict(filter(lambda val: val[1] is not None,query.items()))

	response = client.get('/api/search?'+urlencode(query))
	data = json.loads(response.get_data(as_text=True))

	# ~ print(data)
	assert response.status_code == httpCode

	if httpCode == 200:
		clc = ItemCollection.from_dict(data)

		if validRanks is not None:
			assert len(clc) == len(validRanks)

			if len(validRanks) > 0:
				with psycopg.connect(dburl) as db:
					validIds = db.execute("SELECT array_agg(pic_id ORDER BY rank) FROM sequences_pictures WHERE rank = ANY(%s)", [validRanks]).fetchone()[0]
					allIds = db.execute("SELECT array_agg(pic_id ORDER BY rank) FROM sequences_pictures").fetchone()[0]
					resIds = [ UUID(item.id) for item in clc ]
					print(allIds)
					print(validIds)
					assert sorted(resIds) == sorted(validIds)

					for i in range(len(validRanks)):
						r = validRanks[i]
						id = validIds[i]
						links = [ it.links for it in clc.items if it.id == str(id) ][0]
						if r == 1:
							assert [ l.target.split("/").pop() for l in links if l.rel == "next"] == [str(allIds[r])]
							assert [ l.target.split("/").pop() for l in links if l.rel == "prev"] == []
						elif r == 5:
							assert [ l.target.split("/").pop() for l in links if l.rel == "next"] == []
							assert [ l.target.split("/").pop() for l in links if l.rel == "prev"] == [str(allIds[r-2])]
						else:
							assert [ l.target.split("/").pop() for l in links if l.rel == "next"] == [str(allIds[r])]
							assert [ l.target.split("/").pop() for l in links if l.rel == "prev"] == [str(allIds[r-2])]

		elif limit is not None:
			assert len(clc) == limit


@conftest.SEQ_IMGS
def test_search_post(datafiles, initSequence):
	client = initSequence(datafiles, preprocess=False)

	response = client.post('/api/search', json={'limit': 1, 'intersects': intersectsGeojson1})
	data = json.loads(response.get_data(as_text=True))

	assert response.status_code == 200
	clc = ItemCollection.from_dict(data)
	assert len(clc) == 1


def test_post_collection_nobody(client, dburl):
	response = client.post('/api/collections')

	assert response.status_code == 200
	assert response.headers.get("Location").startswith("http://localhost:5000/api/collections/")
	seqId = UUID(response.headers.get("Location").split("/").pop())
	assert seqId != ""

	# Check if JSON is a valid STAC collection
	assert response.json["type"] == "Collection"
	assert response.json["id"] == str(seqId)
	# the collection is associated to the default account since no auth was done
	assert response.json["providers"] == [{'name': 'Default account', 'roles': ['producer']}]

	# Check if collection exists in DB
	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqFolder, seqStatus = cursor.execute("SELECT folder_path, status FROM sequences WHERE id = %s", [seqId]).fetchone()
			assert seqStatus == "preparing"
			assert seqFolder.startswith("geovisio_uploads_")


def test_post_collection_body_form(client):
	response = client.post('/api/collections', data={ "title": "Séquence" })

	assert response.status_code == 200
	assert response.headers.get("Location").startswith("http://localhost:5000/api/collections/")
	seqId = UUID(response.headers.get("Location").split("/").pop())
	assert seqId != ""

	# Check if JSON is a valid STAC collection
	assert response.json["type"] == "Collection"
	assert response.json["id"] == str(seqId)
	assert response.json["title"] == "Séquence"


def test_post_collection_body_json(client):
	response = client.post('/api/collections', json={ "title": "Séquence" })

	assert response.status_code == 200
	assert response.headers.get("Location").startswith("http://localhost:5000/api/collections/")
	seqId = UUID(response.headers.get("Location").split("/").pop())
	assert seqId != ""

	# Check if JSON is a valid STAC collection
	assert response.json["type"] == "Collection"
	assert response.json["id"] == str(seqId)
	assert response.json["title"] == "Séquence"


@conftest.SEQ_IMG_FLAT
def test_post_item_nobody(datafiles, initSequence, dburl):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId = cursor.execute("SELECT id FROM sequences LIMIT 1").fetchone()[0]

			response = client.post(f"/api/collections/{seqId}/items")
			assert response.status_code == 415


@pytest.mark.parametrize(
	('filename', 'position', 'httpCode'), (
	(   "1.jpg",          2,        202),
	(   "1.jpg",          1,        409),
	(      None,          2,        400),
	(   "1.jpg",         -1,        400),
	(   "1.jpg",      "bla",        400),
	(   "1.txt",          2,        400),
))
@conftest.SEQ_IMG_FLAT
def test_post_item_body_formdata(datafiles, initSequence, dburl, filename, position, httpCode):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId = cursor.execute("SELECT id FROM sequences LIMIT 1").fetchone()[0]

			# Make sequence marked as preparing
			cursor.execute("UPDATE sequences SET status='preparing' WHERE id = %s", [seqId])
			conn.commit()

			if filename is not None and filename != "1.jpg":
				os.mknod(datafiles / "seq1" / filename)

			response = client.post(
				f"/api/collections/{seqId}/items",
				headers = { "Content-Type": "multipart/form-data" },
				data = {
					"position": position,
					"picture": (datafiles / "seq1" / filename).open("rb") if filename is not None else None
				}
			)

			print(response.text)
			assert response.status_code == httpCode

			# Further testing if picture was accepted
			if httpCode == 202:
				assert response.headers.get("Location").startswith(f"http://localhost/api/collections/{seqId}/items/")
				picId = UUID(response.headers.get("Location").split("/").pop())
				assert str(picId) != ""

				# Check the returned JSON
				assert response.json["type"] == "Feature"
				assert response.json["id"] == str(picId)
				assert response.json["collection"] == str(seqId)
				# since the upload was not authenticated, the pictures are associated to the default account
				assert response.json["providers"] == [{'name': 'Default account', 'roles': ['producer']}]

				# Check that picture has been correctly processed
				retries = 0
				while retries < 10 and retries != -1:
					dbStatus = cursor.execute("SELECT status FROM pictures WHERE id = %s", [ picId ]).fetchone()[0]

					if dbStatus == "ready":
						retries = -1
						laterResponse = client.get(f"/api/collections/{seqId}/items/{picId}")
						assert laterResponse.status_code == 200

						# Check sequence is marked as ready
						seqStatus = cursor.execute("SELECT status FROM sequences WHERE id = %s", [seqId]).fetchone()[0]
						assert seqStatus == "ready"
					else:
						retries += 1
						time.sleep(2)

				if retries == 10:
					raise Exception("Picture has never been processed")


def test_getCollectionImportStatus_noseq(client):
	response = client.get(f"/api/collections/00000000-0000-0000-0000-000000000000/geovisio_status")
	assert response.status_code == 404


@conftest.SEQ_IMGS_FLAT
def test_getCollectionImportStatus_ready(datafiles, initSequence, dburl):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId = cursor.execute("SELECT id FROM sequences LIMIT 1").fetchone()[0]

			response = client.get(f"/api/collections/{seqId}/geovisio_status")

			assert response.status_code == 200
			assert len(response.json["items"]) == 2

			for i in response.json["items"]:
				assert len(i) == 3
				assert UUID(i["id"]) is not None
				assert i["rank"] > 0
				assert i["status"] == "ready"


@conftest.SEQ_IMGS_FLAT
def test_getCollectionImportStatus_hidden(datafiles, initSequence, dburl):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId = cursor.execute("SELECT id FROM sequences LIMIT 1").fetchone()[0]
			picId = cursor.execute("SELECT pic_id FROM sequences_pictures WHERE seq_id = %s AND rank = 1", [seqId]).fetchone()[0]
			cursor.execute("UPDATE pictures SET status = 'hidden' WHERE id = %s", [picId])
			conn.commit()

			response = client.get(f"/api/collections/{seqId}/geovisio_status")

			assert response.status_code == 200
			assert len(response.json["items"]) == 1
			assert response.json["items"][0]["id"] != picId
			assert response.json["items"][0]["status"] == "ready"


@conftest.SEQ_IMGS_FLAT
def test_upload_sequence(datafiles, client):
	# Create sequence
	resPostSeq = client.post("/api/collections")
	assert resPostSeq.status_code == 200
	seqId = resPostSeq.json["id"]

	# Create first image
	resPostImg1 = client.post(
		f"/api/collections/{seqId}/items",
		headers = { "Content-Type": "multipart/form-data" },
		data = {
			"position": 1,
			"picture": (datafiles / "b1.jpg").open("rb")
		}
	)

	assert resPostImg1.status_code == 202

	# Create second image
	resPostImg2 = client.post(
		f"/api/collections/{seqId}/items",
		headers = { "Content-Type": "multipart/form-data" },
		data = {
			"position": 2,
			"picture": (datafiles / "b2.jpg").open("rb")
		}
	)

	assert resPostImg2.status_code == 202

	# Check upload status
	retries = 0
	while retries < 10 and retries != -1:
		resGetStatus = client.get(f"/api/collections/{seqId}/geovisio_status")

		assert resGetStatus.status_code == 200
		assert len(resGetStatus.json["items"]) == 2

		if [ s["status"] for s in resGetStatus.json["items"] ] == ["ready", "ready"]:
			retries = -1
		else:
			retries += 1
			time.sleep(2)

	if retries == 10:
		raise Exception("Picture has never been processed")

	# Check sequence is ready
	resGetSeq = client.get(f"/api/collections/{seqId}")
	assert resGetSeq.status_code == 200
